#ifndef _PROGRAM_LOADER_H
#define _PROGRAM_LOADER_H

#include "Vm.hpp"
#include "code_loader/CodeLoader.hpp"
#include <map>
#include <regex>
#include <string>

class ProgramLoader
{
public:
    std::map<std::string, size_t> markers;

    void load(std::string file, CodeLoader& code);
    void load_raw(std::string& file, CodeLoader& code);
    void link(CodeLoader& code);

private:
    static const std::regex marker_expr;
    static const std::regex symbol_expr;

    std::vector<std::pair<std::string, int>> unresolved_symbols;

    template <typename T> void process_opcode(CodeLoader& code, T& it);

    template <typename T>
    void process_symbol_reference(CodeLoader& code, T& it);

    template <typename T>
    void process_symbol_definition(CodeLoader& code, T& it);

    template <typename T> void process_integer(CodeLoader& code, T& it);

    template <typename T> Type process_type(CodeLoader& code, T& it);

    template <typename T> void process_const(CodeLoader& code, T& it);
};

#endif
