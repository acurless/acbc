#ifndef _VM_H
#define _VM_H
#include "VirtualMemory.hpp"
#include "code_loader/CodeLoader.hpp"
#include "debugger/Debugger.hpp"
#include <cstdint>
#include <cstdlib>
#include <functional>
#include <map>
#include <vector>

enum class Instruction : uint8_t
{
    NOP = 0x00,
    JMP,
    JMPT,
    JMPF,
    LDARG,
    ADDR,
    PUTC,
    GETC,
    HALT,
    CALL,
    LD,
    LDI,
    STR,
    STRI,
    PRNT,
    PUSH,
    POP,
    RET,
    ADD,
    SUB,
    MUL,
    COPY,
    LT,
    GT,
    EQ,
    RTL,
    RTR,
    XOR,
    AND,
    OR,
    ASSRT,
    MOD,
    THRD,
    JOIN,
    HSTRT,
    RND
};

enum class Type : uint8_t
{
    BYTE = 0x00,
    INT,
    FLOAT,
};

enum class InstructionClass : uint8_t
{
    UNTYPED = 0x00,
    TYPED,
};

class Vm;
using InstructionInfo =
    std::map<Instruction, std::tuple<InstructionClass, std::string,
                                     std::function<void(Vm&)>>>;

int str_to_opcode(const std::string& instr);

class Vm
{
public:
    static constexpr size_t STACK_SIZE = 16384;
    static constexpr size_t HEAP_SIZE = 16384;
    static constexpr size_t MEM_SIZE = STACK_SIZE + HEAP_SIZE;

    static const std::vector<Instruction>& jump_instructions();
    static const std::map<const std::string, Instruction>& instruction_names();
    static const std::vector<Instruction>& typed_instructions();

    Vm(CodeLoader& code, int pc);
    Vm(CodeLoader& code, int pc, int sp, int fp, VirtualMemory* stack);

    ~Vm();

    void execute_one_instruction();

    bool is_running() const;

    int get_program_counter() const;

private:
    friend class Debugger;
    const bool stack_owner;
    bool running = true;

    int pc;
    int fp;
    int sp;
    int32_t return_value = -1;
    CodeLoader& code;
    VirtualMemory* vmem;

    int fd;
    static const InstructionInfo instrs;

    inline Type determine_type()
    {
        uint8_t type = next_code<uint8_t>();
        return static_cast<Type>(type);
    }

    template <typename F> inline void typed_bifunction_execute(F& opr, Type t);
    template <typename F> void typed_bi_predicate(F& opr, Type t);
    template <typename F> void store_helper(Type t, F& calc_str_addr);
    template <typename F> void load_helper(Type t, F& addr_calculator);
    template <typename T> void ret_helper();
    template <typename T, typename R, typename F>
    inline void bifunction(F& func);
    inline void do_sync(uint32_t thread_id);

    template <typename T> inline T pop();
    template <typename T> inline void push(uint8_t* v);

    inline int pop_int();
    inline uint8_t pop_byte();
    inline void push_int(int* v);
    inline void push_byte(uint8_t* v);

    uint8_t next_code_byte();
    template <typename T> T next_code();

    void addr();
    void instr_putc();
    void instr_getc();
    void jmpt();
    void jmpf();
    void ldarg();
    void call();
    void thrd();
    void sync();
    void halt();
    void hstrt();
    void round();

    void instr_pop(Type t);
    void instr_push(Type t);
    void mod(Type t);
    void eq(Type t);
    void ret(Type t);
    void ld(Type t);
    void ldi(Type t);
    void str(Type t);
    void stri(Type t);
    void copy(Type t);
    void lt(Type t);
    void gt(Type t);
    void print(Type t);
    void add(Type t);
    void sub(Type t);
    void rtl(Type t);
    void rtr(Type t);
    void mul(Type t);
    void instr_xor(Type t);
    void instr_and(Type t);
    void instr_or(Type t);
    void instr_assert(Type t);
};
#endif
