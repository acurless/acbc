#ifndef _EXECUTOR_H
#define _EXECUTOR_H

#include "Vm.hpp"
#include "code_loader/CodeLoader.hpp"

class Executor
{
public:
    Executor(CodeLoader& code, int main_addr)
        : vm(code, main_addr), main_addr(main_addr)
    {
    }

    virtual ~Executor()
    {
    }

    virtual void execute() = 0;

protected:
    Vm vm;
    const int main_addr;
};

#endif
