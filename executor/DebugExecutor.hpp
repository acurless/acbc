#ifndef _DEBUG_EXECTUOR_H
#define _DEBUG_EXECTUOR_H

#include "Executor.hpp"

class DebugExecutor : public Executor
{
public:
    DebugExecutor(CodeLoader& code, int main_addr,
                  std::map<std::string, size_t>& markers);

    ~DebugExecutor();

    void execute() override;

private:
    const std::map<std::string, size_t>& markers;
};

#endif
