#include "DebugExecutor.hpp"
#include "debugger/Debugger.hpp"

DebugExecutor::DebugExecutor(CodeLoader& code, int main_addr,
                             std::map<std::string, size_t>& markers)
    : Executor(code, main_addr), markers(markers)
{
}

DebugExecutor::~DebugExecutor()
{
}

void DebugExecutor::execute()
{
    printf("Debug Mode\n");
    Debugger::get_instance().set_breakpoint(main_addr);
    Debugger::get_instance().init(&vm, this->markers);

    Debugger::get_instance().run();
}
