#include "ProductionExecutor.hpp"

ProductionExecutor::ProductionExecutor(CodeLoader& code, int main_addr)
    : Executor(code, main_addr)
{
}

ProductionExecutor::~ProductionExecutor()
{
}

void ProductionExecutor::execute()
{
    do
    {
        vm.execute_one_instruction();
    } while (vm.is_running());
}
