#ifndef _PRODUCTION_EXECUTOR_H
#define _PRODUCTION_EXECUTOR_H

#include "Executor.hpp"

class ProductionExecutor : public Executor
{
public:
    ProductionExecutor(CodeLoader& code, int main_addr);

    ~ProductionExecutor();

    void execute() override;

private:
};

#endif
