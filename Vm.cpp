#include "Vm.hpp"
#include "ACBCThreadManager.hpp"
#include "Util.hpp"
#include "debugger/Debugger.hpp"
#include <cmath>
#include <cstring>
#include <future>
#include <iostream>
#include <limits.h>

const InstructionInfo Vm::instrs = {
    {Instruction::HALT,
     {InstructionClass::UNTYPED, "HALT", [](Vm& vm) { vm.halt(); }}},
    {Instruction::JMP,
     {InstructionClass::UNTYPED, "JMP",
      [](Vm& vm) { vm.pc = vm.next_code<int>(); }}},
    {Instruction::JMPT,
     {InstructionClass::UNTYPED, "JMPT", [](Vm& vm) { vm.jmpt(); }}},
    {Instruction::JMPF,
     {InstructionClass::UNTYPED, "JMPF", [](Vm& vm) { vm.jmpf(); }}},
    {Instruction::PUTC,
     {InstructionClass::UNTYPED, "PUTC", [](Vm& vm) { vm.instr_putc(); }}},
    {Instruction::GETC,
     {InstructionClass::UNTYPED, "GETC", [](Vm& vm) { vm.instr_getc(); }}},
    {Instruction::ADDR,
     {InstructionClass::UNTYPED, "ADDR", [](Vm& vm) { vm.addr(); }}},
    {Instruction::CALL,
     {InstructionClass::UNTYPED, "CALL", [](Vm& vm) { vm.call(); }}},
    {Instruction::LDARG,
     {InstructionClass::UNTYPED, "LDARG", [](Vm& vm) { vm.ldarg(); }}},
    {Instruction::PRNT,
     {InstructionClass::TYPED, "PRNT",
      [](Vm& vm) { vm.print(vm.determine_type()); }}},
    {Instruction::LD,
     {InstructionClass::TYPED, "LD",
      [](Vm& vm) { vm.ld(vm.determine_type()); }}},
    {Instruction::LDI,
     {InstructionClass::TYPED, "LDI",
      [](Vm& vm) { vm.ldi(vm.determine_type()); }}},
    {Instruction::STR,
     {InstructionClass::TYPED, "STR",
      [](Vm& vm) { vm.str(vm.determine_type()); }}},
    {Instruction::STRI,
     {InstructionClass::TYPED, "STRI",
      [](Vm& vm) { vm.stri(vm.determine_type()); }}},
    {Instruction::RET,
     {InstructionClass::TYPED, "RET",
      [](Vm& vm) { vm.ret(vm.determine_type()); }}},
    {Instruction::EQ,
     {InstructionClass::TYPED, "EQ",
      [](Vm& vm) { vm.eq(vm.determine_type()); }}},
    {Instruction::SUB,
     {InstructionClass::TYPED, "SUB",
      [](Vm& vm) { vm.sub(vm.determine_type()); }}},
    {Instruction::PUSH,
     {InstructionClass::TYPED, "PUSH",
      [](Vm& vm) { vm.instr_push(vm.determine_type()); }}},
    {Instruction::POP,
     {InstructionClass::TYPED, "POP",
      [](Vm& vm) { vm.instr_pop(vm.determine_type()); }}},
    {Instruction::ADD,
     {InstructionClass::TYPED, "ADD",
      [](Vm& vm) { vm.add(vm.determine_type()); }}},
    {Instruction::COPY,
     {InstructionClass::TYPED, "COPY",
      [](Vm& vm) { vm.copy(vm.determine_type()); }}},
    {Instruction::LT,
     {InstructionClass::TYPED, "LT",
      [](Vm& vm) { vm.lt(vm.determine_type()); }}},
    {Instruction::GT,
     {InstructionClass::TYPED, "GT",
      [](Vm& vm) { vm.gt(vm.determine_type()); }}},
    {Instruction::RTL,
     {InstructionClass::TYPED, "RTL",
      [](Vm& vm) { vm.rtl(vm.determine_type()); }}},
    {Instruction::RTR,
     {InstructionClass::TYPED, "RTR",
      [](Vm& vm) { vm.rtr(vm.determine_type()); }}},
    {Instruction::MUL,
     {InstructionClass::TYPED, "MUL",
      [](Vm& vm) { vm.mul(vm.determine_type()); }}},
    {Instruction::XOR,
     {InstructionClass::TYPED, "XOR",
      [](Vm& vm) { vm.instr_xor(vm.determine_type()); }}},
    {Instruction::AND,
     {InstructionClass::TYPED, "AND",
      [](Vm& vm) { vm.instr_and(vm.determine_type()); }}},
    {Instruction::OR,
     {InstructionClass::TYPED, "OR",
      [](Vm& vm) { vm.instr_or(vm.determine_type()); }}},
    {Instruction::ASSRT,
     {InstructionClass::TYPED, "ASSRT",
      [](Vm& vm) { vm.instr_assert(vm.determine_type()); }}},
    {Instruction::MOD,
     {InstructionClass::TYPED, "MOD",
      [](Vm& vm) { vm.mod(vm.determine_type()); }}},
    {Instruction::THRD,
     {InstructionClass::UNTYPED, "THRD", [](Vm& vm) { vm.thrd(); }}},
    {Instruction::JOIN,
     {InstructionClass::UNTYPED, "JOIN", [](Vm& vm) { vm.sync(); }}},
    {Instruction::HSTRT,
     {InstructionClass::UNTYPED, "HSTRT", [](Vm& vm) { vm.hstrt(); }}},
    {Instruction::RND,
     {InstructionClass::UNTYPED, "RND", [](Vm& vm) { vm.round(); }}}};

template <typename T, typename F>
T acbc_operation(void* a, void* b, size_t t, F func)
{

    if (t == sizeof(int))
    {
        int result = func(*static_cast<int*>(b), *static_cast<int*>(a));
        T ret;
        memcpy(&ret, &result, sizeof(T));
        return ret;
    }
    else if (t == sizeof(uint8_t))
    {
        return func(*(uint8_t*)a, *(uint8_t*)b);
    }
    else
    {
        throw std::runtime_error("Unresolved type of size " +
                                 std::to_string(t));
    }
}

const std::map<const std::string, Instruction>& Vm::instruction_names()
{
    static std::map<const std::string, Instruction> names;

    if (names.empty())
    {
        for (auto it = Vm::instrs.cbegin(); it != Vm::instrs.cend(); ++it)
        {
            names.emplace(std::get<1>(it->second), it->first);
        }
    }

    return names;
}

const std::vector<Instruction>& Vm::typed_instructions()
{
    static std::vector<Instruction> typed;
    if (typed.empty())
    {
        for (auto it = Vm::instrs.cbegin(); it != Vm::instrs.cend(); ++it)
        {
            if (std::get<0>(it->second) == InstructionClass::TYPED)
            {
                typed.push_back(it->first);
            }
        }
    }
    return typed;
}

const std::vector<Instruction>& Vm::jump_instructions()
{
    static std::vector<Instruction> jmps;
    if (jmps.empty())
    {
        for (auto it = Vm::instrs.cbegin(); it != Vm::instrs.cend(); ++it)
        {
            auto& name = std::get<1>(it->second);
            if (name.find("JMP") != std::string::npos)
            {
                jmps.push_back(it->first);
            }
        }
    }
    return jmps;
}

int str_to_opcode(const std::string& instr)
{
    int ret = -1;
    auto instr_str = Vm::instruction_names().find(instr);
    if (instr_str != Vm::instruction_names().end())
    {
        ret = static_cast<int>(instr_str->second);
    }
    else
    {
        program_terminate("Unknown instruction " + instr);
    }
    return ret;
}

Vm::Vm(CodeLoader& code, int pc)
    : stack_owner(true), pc(pc), fp(0), sp(0), code(code),
      vmem(new VirtualMemory(STACK_SIZE, HEAP_SIZE)), fd(1)
{
    this->vmem->setmem(0, 0, MEM_SIZE);
}

Vm::Vm(CodeLoader& code, int pc, int sp, int fp, VirtualMemory* vmem)
    : stack_owner(false), pc(pc), fp(fp), sp(sp), code(code), vmem(vmem), fd(1)
{
}

Vm::~Vm()
{
    if (this->stack_owner)
    {
        delete this->vmem;
    }
}

bool Vm::is_running() const
{
    return this->running;
}

int Vm::get_program_counter() const
{
    return this->pc;
}

void Vm::execute_one_instruction()
{
    uint8_t opcode = next_code<uint8_t>();
    Instruction instr = static_cast<Instruction>(opcode);
    if (this->instrs.find(instr) != this->instrs.end())
    {
        std::get<2>(this->instrs.at(instr))(*this);
    }
    else
    {
        printf("Unknown Instruction %02X\n", opcode);
        std::exit(1);
    }
}

template <typename T, typename R, typename F> void Vm::bifunction(F& func)
{
    T b = pop<T>();
    T a = pop<T>();
    R c = func(a, b);
    push<R>((uint8_t*)&c);
}

template <typename F> void Vm::typed_bifunction_execute(F& opr, Type t)
{
    switch (t)
    {
    case Type::BYTE:
    {
        this->bifunction<int8_t, int8_t>(opr);
        break;
    }
    case Type::INT:
    {
        this->bifunction<int32_t, int32_t>(opr);
        break;
    }
    case Type::FLOAT:
    {
        this->bifunction<float, float>(opr);
        break;
    }
    }
}

template <typename F> void Vm::typed_bi_predicate(F& opr, Type t)
{
    switch (t)
    {
    case Type::BYTE:
    {
        this->bifunction<int8_t, int8_t>(opr);
        break;
    }
    case Type::INT:
    {
        this->bifunction<int32_t, int8_t>(opr);
        break;
    }
    case Type::FLOAT:
    {
        this->bifunction<float, int8_t>(opr);
        break;
    }
    }
}

template <typename F> void Vm::store_helper(Type t, F& calc_str_addr)
{
    uint32_t offset = pop<uint32_t>();
    switch (t)
    {
    case Type::BYTE:
    {
        int8_t val = pop_byte();
        memcpy(calc_str_addr(offset), &val, sizeof(int8_t));
        break;
    }
    case Type::INT:
    {
        int32_t val = pop_int();
        memcpy(calc_str_addr(offset), &val, sizeof(int32_t));
        break;
    }
    case Type::FLOAT:
    {
        float val = pop<float>();
        memcpy(calc_str_addr(offset), &val, sizeof(float));
        break;
    }
    }
}

template <typename F> void Vm::load_helper(Type t, F& addr_calculator)
{
    int32_t offset = pop_int();
    switch (t)
    {
    case Type::BYTE:
    {
        push_byte(addr_calculator(offset));
        break;
    }
    case Type::INT:
    {
        push<int32_t>(addr_calculator(offset));
        break;
    }
    case Type::FLOAT:
    {
        push<float>(addr_calculator(offset));
        break;
    }
    }
}

void Vm::eq(Type t)
{
    const auto func = [](auto& a, auto& b) { return a == b; };
    typed_bi_predicate(func, t);
}

template <typename T> void Vm::ret_helper()
{
    T rval = pop<T>();
    sp = fp;

    pc = pop<int32_t>();
    fp = pop<int32_t>();
    int32_t argc = pop<int32_t>();

    if (pc == INT_MAX && fp == INT_MAX && argc == INT_MAX)
    {
        // Magic values for a threaded context. Need to terminate this
        // thread.
        this->running = false;
        this->return_value = rval;
    }
    else
    {
        sp -= argc * sizeof(int32_t);
        push<T>((uint8_t*)&rval);
    }
}

void Vm::ret(Type t)
{
    switch (t)
    {
    case Type::BYTE:
    {
        ret_helper<int8_t>();
        break;
    }
    case Type::INT:
    {
        ret_helper<int32_t>();
        break;
    }
    case Type::FLOAT:
    {
        ret_helper<float>();
        break;
    }
    }
}

void Vm::call()
{
    int addr = next_code<int>();
    int32_t argc = next_code<int>();
    push<int32_t>((uint8_t*)&argc);
    push<int32_t>((uint8_t*)&fp);
    push<int32_t>((uint8_t*)&pc);
    fp = sp;
    pc = addr;
}

void Vm::ld(Type t)
{
    const auto calculator = [this](int32_t offset) {
        return this->vmem->add(this->fp + offset);
    };
    load_helper(t, calculator);
}

void Vm::ldi(Type t)
{
    const auto calculator = [this](int32_t offset) {
        return this->vmem->add(offset);
    };
    load_helper(t, calculator);
}

inline int Vm::pop_int()
{
    return pop<int32_t>();
}

inline uint8_t Vm::pop_byte()
{
    return pop<int8_t>();
}

inline void Vm::push_int(int* v)
{
    push<int32_t>((uint8_t*)v);
}

inline void Vm::push_byte(uint8_t* v)
{
    push<int8_t>(v);
}

void Vm::ldarg()
{
    int arg = next_code<int>();
    int32_t offset = -1 * ((3 + arg) * sizeof(int32_t));
    push<int32_t>(this->vmem->add(fp + offset));
}

void Vm::str(Type t)
{
    const auto addr_calculator = [this](uint32_t offset) {
        return this->vmem->add(this->fp + offset);
    };
    store_helper(t, addr_calculator);
}

void Vm::stri(Type t)
{
    const auto addr_calculator = [this](uint32_t offset) {
        return this->vmem->add(offset);
    };
    store_helper(t, addr_calculator);
}

void Vm::copy(Type t)
{
    switch (t)
    {
    case Type::BYTE:
        push<int8_t>(this->vmem->add(sp - sizeof(int8_t)));
        break;
    case Type::INT:
        push<int32_t>(this->vmem->add(sp - sizeof(int32_t)));
        break;
    case Type::FLOAT:
        push<float>(this->vmem->add(sp - sizeof(float)));
        break;
    }
}

void Vm::addr()
{
    int offset = next_code<int>();

    uint32_t ret = fp + offset;
    push<uint32_t>((uint8_t*)&ret);
}

void Vm::jmpt()
{
    int addr = next_code<int>();
    if (pop<int8_t>())
    {
        this->pc = addr;
    }
}

void Vm::jmpf()
{
    int addr = next_code<int>();
    if (!pop<int8_t>())
    {
        this->pc = addr;
    }
}

void Vm::lt(Type t)
{
    const auto func = [](auto& a, auto& b) { return a < b ? 1 : 0; };
    typed_bi_predicate(func, t);
}

void Vm::gt(Type t)
{
    const auto func = [](auto& a, auto& b) { return a > b ? 1 : 0; };
    typed_bi_predicate(func, t);
}

void Vm::print(Type t)
{
    switch (t)
    {
    case Type::BYTE:
    {
        int8_t v = pop<int8_t>();
        dprintf(fd, "%d\n", v);
        break;
    }
    case Type::INT:
    {
        int32_t v = pop<int32_t>();
        dprintf(fd, "%d\n", v);
        break;
    }
    case Type::FLOAT:
    {
        float v = pop<float>();
        dprintf(fd, "%f\n", v);
        break;
    }
    }
}

void Vm::instr_putc()
{
    char v = pop<char>();
    dprintf(fd, "%c", v);
}

void Vm::instr_getc()
{
    uint8_t c = fgetc(stdin);
    push<uint8_t>(&c);
}

void Vm::instr_push(Type t)
{
    switch (t)
    {
    case Type::BYTE:
    {
        uint8_t val = next_code<uint8_t>();
        push<int8_t>(&val);
        break;
    }
    case Type::INT:
    {
        int32_t val = next_code<int32_t>();
        push<int32_t>((uint8_t*)&val);
        break;
    }
    case Type::FLOAT:
    {
        float val = next_code<float>();
        push<float>((uint8_t*)&val);
        break;
    }
    }
}

void Vm::instr_pop(Type t)
{
    switch (t)
    {
    case Type::BYTE:
        pop<int8_t>();
        break;
    case Type::INT:
        pop<int32_t>();
        break;
    case Type::FLOAT:
        pop<float>();
        break;
    }
}

void Vm::add(Type t)
{
    const auto func = [](auto& a, auto& b) { return a + b; };
    typed_bifunction_execute(func, t);
}

void Vm::sub(Type t)
{
    const auto func = [](auto& a, auto& b) { return a - b; };
    typed_bifunction_execute(func, t);
}

void Vm::instr_xor(Type t)
{
    const auto func = [](auto a, auto b) {
        return acbc_operation<decltype(a)>(
            &a, &b, sizeof(decltype(a)), [](auto a, auto b) { return a ^ b; });
    };
    typed_bifunction_execute(func, t);
}

void Vm::instr_and(Type t)
{
    const auto func = [](auto a, auto b) {
        return acbc_operation<decltype(a)>(
            &a, &b, sizeof(decltype(a)), [](auto a, auto b) { return a & b; });
    };
    typed_bifunction_execute(func, t);
}

void Vm::instr_or(Type t)
{
    const auto func = [](auto a, auto b) {
        return acbc_operation<decltype(a)>(
            &a, &b, sizeof(decltype(a)), [](auto a, auto b) { return a | b; });
    };
    typed_bifunction_execute(func, t);
}

uint8_t Vm::next_code_byte()
{
    return this->code[pc++];
}

template <typename T> T Vm::next_code()
{
    uint8_t ret[sizeof(T)];
    for (size_t i = 0; i < sizeof(T); i++)
    {
        ret[i] = next_code_byte();
    }
    return *(T*)ret;
}

void Vm::rtl(Type t)
{
    const auto func = [](auto a, auto b) {
        return acbc_operation<decltype(a)>(
            &a, &b, sizeof(decltype(a)), [](auto a, auto b) { return a << b; });
    };
    typed_bifunction_execute(func, t);
}

void Vm::rtr(Type t)
{
    const auto func = [](auto a, auto b) {
        return acbc_operation<decltype(a)>(
            &a, &b, sizeof(decltype(a)), [](auto a, auto b) { return a >> b; });
    };
    typed_bifunction_execute(func, t);
}

void Vm::mul(Type t)
{
    const auto f_multiply = [](auto& a, auto& b) { return a * b; };
    typed_bifunction_execute(f_multiply, t);
}

void Vm::thrd()
{
    int jump_addr = next_code<int>();

    const size_t new_stack_ptr = this->sp + (STACK_SIZE / 6);
    if (new_stack_ptr < STACK_SIZE)
    {
        std::unique_ptr<Vm> new_thread = std::make_unique<Vm>(
            code, jump_addr, new_stack_ptr, new_stack_ptr, this->vmem);

        std::future<int32_t> f = std::async(
            std::launch::async, [new_thread = std::move(new_thread)]() {
                constexpr int32_t max = INT_MAX;
                new_thread->push<int32_t>((uint8_t*)&max);
                new_thread->push<int32_t>((uint8_t*)&max);
                new_thread->push<int32_t>((uint8_t*)&max);
                new_thread->fp = new_thread->sp;

                do
                {
                    new_thread->execute_one_instruction();
                } while (new_thread->is_running());
                return new_thread->return_value;
            });
        uint32_t thread_id = ACBCThreadManager::get_instance().new_thread(
            std::move(new_thread), std::move(f));

        push<uint32_t>((uint8_t*)&thread_id);
    }
    else
    {
        program_terminate("PANIC: Stack not large enough to support fork");
    }
}

void Vm::do_sync(uint32_t thread_id)
{
    // Thread_id 0 indicates that all threads should be joined.
    if (thread_id == 0)
    {
        std::vector<uint32_t> active =
            ACBCThreadManager::get_instance().active_threads();
        for (const uint32_t value : active)
        {
            do_sync(value);
        }
    }
    else
    {
        int32_t rval = ACBCThreadManager::get_instance().join_thread(thread_id);
        push<int32_t>((uint8_t*)&rval);
    }
}

void Vm::sync()
{
    uint32_t thread_id = pop<uint32_t>();
    do_sync(thread_id);
}

void Vm::halt()
{
    this->running = false;

    std::vector<uint32_t> active =
        ACBCThreadManager::get_instance().active_threads();
    for (const uint32_t value : active)
    {
        do_sync(value);
    }
}

void Vm::instr_assert(Type t)
{
    const auto f_assert = [](auto& a, auto& b) {
        if (a == b)
        {
            return 0;
        }
        else
        {
            exit(-1);
        }
    };
    typed_bifunction_execute(f_assert, t);
}

void Vm::mod(Type t)
{
    const auto func = [](auto a, auto b) {
        return acbc_operation<decltype(a)>(
            &a, &b, sizeof(decltype(a)), [](auto a, auto b) { return a % b; });
    };
    typed_bifunction_execute(func, t);
}

void Vm::hstrt()
{
    push<uint32_t>((uint8_t*)&Vm::STACK_SIZE);
}

void Vm::round()
{
    float val = pop<float>();
    int rounded = std::round(val);
    push<int>((uint8_t*)&rounded);
}

template <typename T> T Vm::pop()
{
    T ret;
    sp -= (sizeof(T));
    memcpy(&ret, this->vmem->add(sp), sizeof(T));
    return ret;
}

template <typename T> void Vm::push(uint8_t* v)
{
    this->vmem->cpymem(sp, v, sizeof(T));
    sp += sizeof(T);
}
