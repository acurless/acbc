#include "ProgramLoader.hpp"
#include "Util.hpp"
#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>

const std::regex ProgramLoader::marker_expr{"[a-z].+:",
                                            std::regex_constants::ECMAScript};
const std::regex ProgramLoader::symbol_expr{"sym\\.[a-z_]+",
                                            std::regex_constants::ECMAScript};

static const std::map<std::string, Type> type_aliases = {
    {"B", Type::BYTE},
    {"I", Type::INT},
    {"F", Type::FLOAT},
};

void ltrim(std::string& s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                                    [](int ch) { return !std::isspace(ch); }));
}

void rtrim(std::string& s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         [](int ch) { return !std::isspace(ch); })
                .base(),
            s.end());
}

void ctrim(std::string& s)
{
    s.erase(std::find_if(s.begin(), s.end(), [](int ch) { return ch == '#'; }),
            s.end());
}

void trim(std::string& s)
{
    ltrim(s);
    rtrim(s);
    ctrim(s);
}

std::string strip_symbol(std::string symbol)
{
    return symbol.substr(4);
}

void ProgramLoader::load_raw(std::string& file, CodeLoader& code)
{
    std::ifstream source;
    source.open(file, std::ios::in);

    if (source.is_open())
    {
        while (source.good())
        {
            code.push_back(source.get());
        }
        source.close();
    }
}

void ProgramLoader::load(std::string file, CodeLoader& code)
{
    std::ifstream source;

    source.open(file, std::ios::in);

    if (source.is_open())
    {
        std::string line;
        while (getline(source, line))
        {
            trim(line);
            std::vector<std::string> ops = tokenizer(line, ' ');
            for (auto it = ops.begin(); it != ops.end(); ++it)
            {
                trim(*it);
                if (!it->empty())
                {
                    if (std::regex_match(*it, marker_expr))
                    {
                        process_symbol_definition(code, it);
                    }
                    else if (std::regex_match(*it, symbol_expr))
                    {
                        process_symbol_reference(code, it);
                    }
                    else
                    {
                        process_opcode(code, it);
                    }
                }
            }
        }
        source.close();
    }
}

template <typename T>
void ProgramLoader::process_symbol_definition(CodeLoader& code, T& it)
{
    markers[it->substr(0, it->size() - 1)] = code.size();
}

template <typename T>
void ProgramLoader::process_symbol_reference(CodeLoader& code, T& it)
{
    unresolved_symbols.push_back({strip_symbol(*it), code.size()});
    for (size_t i = 0; i < sizeof(int); i++)
    {
        code.push_back(0);
    }
}

template <typename T>
void ProgramLoader::process_opcode(CodeLoader& code, T& it)
{
    int op = str_to_opcode(*it);
    code.push_back(op);

    if (op == static_cast<int>(Instruction::PUSH))
    {
        it++;
        Type type = process_type(code, it);
        it++;
        if (is_integer(*it), 10)
        {
            char* p;

            if (type == Type::BYTE)
            {
                int instr = std::strtol(it->c_str(), &p, 10);
                code.push_back(instr);
            }
            else if (type == Type::INT)
            {
                int instr = std::strtol(it->c_str(), &p, 10);
                uint8_t* r = (uint8_t*)&instr;
                for (size_t i = 0; i < sizeof(int); i++)
                {
                    code.push_back(r[i]);
                }
            }
            else if (type == Type::FLOAT)
            {
                float val = std::strtof(it->c_str(), &p);
                uint8_t* r = (uint8_t*)&val;
                for (size_t i = 0; i < sizeof(int); i++)
                {
                    code.push_back(r[i]);
                }
            }
        }

        else
        {
            program_terminate("PUSH and LDARG instructions must be "
                              "followed by an integer value");
        }
    }
    else if (op == static_cast<int>(Instruction::LDARG))
    {
        it++;
        if (is_integer(*it), 10)
        {
            char* p;
            int instr = std::strtol(it->c_str(), &p, 10);
            uint8_t* r = (uint8_t*)&instr;
            for (size_t i = 0; i < sizeof(int); i++)
            {
                code.push_back(r[i]);
            }
        }
        else
        {
            program_terminate("PUSH and LDARG instructions must be "
                              "followed by an integer value");
        }
    }
    else if (op == static_cast<int>(Instruction::JMP) ||
             op == static_cast<int>(Instruction::JMPF) ||
             op == static_cast<int>(Instruction::JMPT) ||
             op == static_cast<int>(Instruction::THRD))
    {
        it++;
        process_symbol_reference(code, it);
    }
    else if (op == static_cast<int>(Instruction::CALL))
    {
        it++;
        process_symbol_reference(code, it);
        it++;
        process_integer(code, it);
    }
    else if (op == static_cast<int>(Instruction::ADDR))
    {
        it++;
        process_integer(code, it);
    }
    else if (std::find(Vm::typed_instructions().begin(),
                       Vm::typed_instructions().end(),
                       static_cast<Instruction>(op)) !=
             Vm::typed_instructions().end())
    {
        it++;
        process_type(code, it);
    }
}

template <typename T> Type ProgramLoader::process_type(CodeLoader& code, T& it)
{
    trim(*it);
    auto type_iter = type_aliases.find(*it);
    if (type_iter != type_aliases.end())
    {
        code.push_back(static_cast<uint8_t>(type_iter->second));
        return type_iter->second;
    }
    else
    {
        program_terminate(
            "Parse Error: Instruction must be followed by a type");
        // NO-OP for linting.
        return Type::BYTE;
    }
}

template <typename T>
void ProgramLoader::process_integer(CodeLoader& code, T& it)
{
    if (is_integer(*it), 10)
    {
        char* p;
        int instr = std::strtol(it->c_str(), &p, 10);
        uint8_t* r = (uint8_t*)&instr;
        for (size_t i = 0; i < sizeof(int); i++)
        {
            code.push_back(r[i]);
        }
    }
}

void ProgramLoader::link(CodeLoader& code)
{
    for (auto it = unresolved_symbols.begin(); it != unresolved_symbols.end();
         ++it)
    {
        if (markers.find(it->first) == markers.end())
        {
            program_terminate("Unresolved symbol " + it->first);
        }
        else
        {
            uint8_t* tmp = (uint8_t*)&markers[it->first];
            size_t idx = it->second;
            for (size_t i = 0; i < sizeof(int); i++)
            {
                code[idx] = tmp[i];
                idx++;
            }
        }
    }
}
