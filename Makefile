export CC=g++
export DEBUG_LIB=$(shell pwd)/debugger.a
export EXECUTOR_LIB=$(shell pwd)/executor.a
export CODELOADER_LIB=$(shell pwd)/code_loader.a
export OBJECT_DIR=$(shell pwd)/obj
export CFLAGS=-Wall -Werror -Wextra -Ofast -fPIC -std=c++17

LINK :=-s -Wl,--build-id -lncurses -ltinfo -lpthread
DESTDIR :=/usr

export DOCDIR=$(DESTDIR)/share
export LIBDIR=$(DOCDIR)/acbc

INCLUDE := -I. -I./executor
SRC = $(wildcard *.cpp)

OBJ = $(patsubst %.cpp,$(OBJECT_DIR)/%.o,$(SRC))

ARTIFACT := acbc

.PHONY: all src debugger $(ARTIFACT)
all: src

$(OBJECT_DIR)/%.o: %.cpp
	$(CC) -c $(INCLUDE) -o $@ $< $(CFLAGS)

$(ARTIFACT): $(OBJ)
	$(CC) -o $@ $^ $(LINK) $(DEBUG_LIB) $(EXECUTOR_LIB) $(CODELOADER_LIB)

init:
	@mkdir -p $(OBJECT_DIR)/debugger
	@mkdir -p $(OBJECT_DIR)/executor
	@mkdir -p $(OBJECT_DIR)/code_loader

subproj:
	$(MAKE) -C debugger
	$(MAKE) -C executor
	$(MAKE) -C code_loader

src: init subproj $(ARTIFACT)

clean:
	rm -rf $(OBJECT_DIR) $(ARTIFACT) *.a
	$(MAKE) -C debugger clean
	$(MAKE) -C executor clean
	$(MAKE) -C code_loader clean

copy: stdlib.acbc
	@mkdir -p $(LIBDIR)
	install -g 0 -o 0 -m 0644 $^ $(LIBDIR)/$^

install: copy
	install -g 0 -o 0 -m 0755 $(ARTIFACT) $(DESTDIR)/bin/$(ARTIFACT)
	$(MAKE) -C man
