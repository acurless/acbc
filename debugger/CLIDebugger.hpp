#ifndef _CLI_DEBUGGER_H
#define _CLI_DEBUGGER_H

#include "DebuggerFrontend.hpp"
#include "debugger/Debugger.hpp"
#include <functional>
#include <map>
#include <vector>

class CLIDebugger : public DebuggerFrontend
{
public:
    CLIDebugger();

    DFStatus run() override;

    void attach_bp(int code_index);

    // Allows the frontend to grab relevant debugger info.
    bool run_command(db_command cmd);

private:
    const std::map<std::string, std::function<bool(db_command)>> args = {
        {"?", [this](db_command argv) { return print_help(argv); }},
        {"w", [this](db_command argv) { return write(argv); }},
        {"p", [this](db_command argv) { return dump(argv); }},
        {"d", [this](db_command argv) { return debug(argv); }},
        {"q", [this](db_command argv) { return terminate(argv); }},
        {"v", [this](db_command argv) { return visual(argv); }},
        {"a", [this](db_command argv) { return analyze(argv); }}};

    bool print_help(db_command cmd);
    bool write(db_command cmd);
    bool dump(db_command argv);
    bool debug(db_command argv);
    bool terminate(db_command argv);
    bool analyze(db_command argv);
    bool visual(db_command argv);

    void display_symbol(size_t& prog_offset);
    void stack_write(db_command cmd);
    void code_write(db_command cmd);
    void print_top_stack(size_t len);
};

#endif
