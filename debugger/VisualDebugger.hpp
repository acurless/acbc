#ifndef _VISUAL_DEBUGGER_H
#define _VISUAL_DEBUGGER_H

#include "DebuggerFrontend.hpp"
#include "Vm.hpp"
#include "debugger/Debugger.hpp"
#include <algorithm>
#include <curses.h>
#include <functional>
#include <locale.h>
#include <map>
#include <memory>
#include <pthread.h>
#include <string>
#include <tuple>
#include <vector>

enum class Color : int16_t
{
    BLACK = 0,
    RED,
    GREEN,
    YELLOW,
    BLUE,
    MAGENTA,
    CYAN,
    WHITE,

    BLACK_INV,
    RED_INV,
    GREEN_INV,
    YELLOW_INV,
    BLUE_INV,
    MAGENTA_INV,
    CYAN_INV,
    WHITE_INV
};

using colorstring = std::tuple<std::string, Color>;
using Arrow = std::vector<uint8_t>;

class VDElement
{
public:
    VDElement(int _x, int _y, bool _visible, int _priority);
    virtual ~VDElement(){};

    static constexpr int COMMAND_SUCCESS = 0;
    static constexpr int COMMAND_UNKNOWN = 1;
    static constexpr int COMMAND_HALT = 2;
    static constexpr int COMMAND_EXIT = 3;

    virtual uint32_t wake(uint32_t ch);
    virtual bool handle_input(uint32_t ch);

    int update_debugger();

    virtual void draw(){};

    bool is_visible();
    int get_priority();

protected:
    const std::map<std::string, std::function<void()>> formatters = {
        {"$hl$", [this]() { highlight_text = true; }},
        {"$/hl$", [this]() { highlight_text = false; }}};

    const std::map<std::string, Color> colors = {
        {"$black$", Color::BLACK}, {"$red$", Color::RED},
        {"$green$", Color::GREEN}, {"$yellow$", Color::YELLOW},
        {"$blue$", Color::BLUE},   {"$magenta$", Color::MAGENTA},
        {"$cyan$", Color::CYAN},   {"$white$", Color::WHITE}};

    const std::map<std::string, std::function<int(db_command)>> args = {
        {"?", [this](db_command argv) { return print_help(argv); }},
        {"w", [this](db_command argv) { return write(argv); }},
        {"p", [this](db_command argv) { return dump(argv); }},
        {"d", [this](db_command argv) { return debug(argv); }},
        {"q", [this](db_command argv) { return close(argv); }}};

    virtual int print_help(db_command cmd);
    virtual int write(db_command cmd);
    virtual int dump(db_command argv);
    virtual int debug(db_command cmd);
    virtual int close(db_command cmd);

    int run_command(db_command cmd);

    void draw_border();

    template <typename... Args>
    std::string write_to_string(const char* format, Args&&... args);
    std::vector<std::string> find_valid_tokens(std::string& line);
    std::vector<colorstring> resolve_colors(std::string& line);
    int get_length(std::string s);
    Color invert_color(Color c);

    int x, y;

    bool highlight_text;

    bool visible;
    int priority; // 0 is highest priority
};

class VDInstructionView : public VDElement
{
public:
    VDInstructionView(int y);
    virtual ~VDInstructionView(){};

    uint32_t wake(uint32_t ch) override;
    bool handle_input(uint32_t ch) override;

    void draw() override;

private:
    class VDInstruction
    {
    public:
        VDInstruction(size_t i, Instruction n, std::string l)
            : idx(i), instruction(n), line(l){};
        virtual ~VDInstruction(){};

        static constexpr uint8_t EMPTY = 0;
        static constexpr uint8_t DOWN = 1;
        static constexpr uint8_t UP = 2;
        static constexpr uint8_t HORIZONTAL = 3;
        static constexpr uint8_t TOP_ARROW = 4;
        static constexpr uint8_t BOTTOM_ARROW = 5;
        static constexpr uint8_t TOP_CORNER = 6;
        static constexpr uint8_t BOTTOM_CORNER = 7;

        size_t idx;
        Instruction instruction;
        std::string line;
        size_t jmp_idx;
        std::vector<uint8_t> arrows;
    };

    static constexpr int MAX_INSTRUCTION_SIZE = 6;

    const std::map<Instruction, std::function<std::string(size_t&)>> writers = {
        {Instruction::JMP, [this](size_t& idx) { return write_jmp(idx); }},
        {Instruction::JMPT, [this](size_t& idx) { return write_jmp(idx); }},
        {Instruction::JMPF, [this](size_t& idx) { return write_jmp(idx); }},
        {Instruction::ADDR, [this](size_t& idx) { return write_jmp(idx); }},
        {Instruction::PUSH, [this](size_t& idx) { return write_push(idx); }},
        {Instruction::LDARG, [this](size_t& idx) { return write_ldarg(idx); }},
        {Instruction::CALL, [this](size_t& idx) { return write_call(idx); }}};

    const std::map<uint8_t, std::function<std::string(std::vector<uint8_t>&)>>
        arrow_drawers = {
            {VDInstruction::EMPTY,
             [this](std::vector<uint8_t>& a) { return ad_empty(a); }},
            {VDInstruction::DOWN,
             [this](std::vector<uint8_t>& a) { return ad_down(a); }},
            {VDInstruction::UP,
             [this](std::vector<uint8_t>& a) { return ad_up(a); }},
            {VDInstruction::HORIZONTAL,
             [this](std::vector<uint8_t>& a) { return ad_horizontal(a); }},
            {VDInstruction::TOP_ARROW,
             [this](std::vector<uint8_t>& a) { return ad_top_arrow(a); }},
            {VDInstruction::BOTTOM_ARROW,
             [this](std::vector<uint8_t>& a) { return ad_bottom_arrow(a); }},
            {VDInstruction::TOP_CORNER,
             [this](std::vector<uint8_t>& a) { return ad_top_corner(a); }},
            {VDInstruction::BOTTOM_CORNER,
             [this](std::vector<uint8_t>& a) { return ad_bottom_corner(a); }},
        };

    std::vector<VDInstruction> codes;
    std::vector<std::string> instructions;
    size_t top_idx;

    std::string write_jmp(size_t& idx);
    std::string write_push(size_t& idx);
    std::string write_ldarg(size_t& idx);
    std::string write_call(size_t& idx);

    std::string ad_empty(std::vector<uint8_t>& a);
    std::string ad_down(std::vector<uint8_t>& a);
    std::string ad_up(std::vector<uint8_t>& a);
    std::string ad_horizontal(std::vector<uint8_t>& a);
    std::string ad_top_arrow(std::vector<uint8_t>& a);
    std::string ad_bottom_arrow(std::vector<uint8_t>& a);
    std::string ad_top_corner(std::vector<uint8_t>& a);
    std::string ad_bottom_corner(std::vector<uint8_t>& a);

    void expand_code(std::string& code);

    void write_instruction(std::string& code, std::string& code_raw,
                           size_t& idx);
    void write_type(std::string& instr, std::string& code, size_t& idx);

    template <typename T> void update_jump_paths(T s, T e, bool direction);
    void find_jump_paths();

    void write_codes();
    void write_code(size_t& i);

    std::string render_instruction(VDInstruction instr);
};

class VDCommandBar : public VDElement
{
public:
    VDCommandBar(int& _fd);
    virtual ~VDCommandBar(){};

    uint32_t wake(uint32_t ch) override;
    bool handle_input(uint32_t ch) override;

    void draw() override;

private:
    static constexpr size_t HISTORY_BEGIN = -1;

    std::string buf;
    std::vector<std::vector<colorstring>> output;
    std::vector<std::string> history;
    std::string tmp_buf;
    size_t history_idx;
    int fd;
    bool ignore_ch;

    void handle_command();

    template <typename... Args>
    void log(const std::string& format, Args&&... args);

    int print_help(db_command cmd) override;

    int debug(db_command cmd) override;

    int dump(db_command argv) override;
    void print_stack(int length, int offset);

    int write(db_command cmd) override;
    void code_write(db_command cmd);
    void stack_write(db_command cmd);

    int close(db_command cmd) override;

    bool last_history(std::string& buf);
    bool next_history(std::string& buf);

    void read_from_stdout();
};

class VisualDebugger : public DebuggerFrontend
{
public:
    VisualDebugger();
    virtual ~VisualDebugger();

    DFStatus run() override;

private:
    int out_pipe[2];

    VDCommandBar* cmdbar;
    VDInstructionView* iview;

    VDElement* focus;
    std::vector<VDElement*> elements;

    uint32_t handle_sequence_char(uint32_t ch);
    void register_elements();
    uint8_t read_input();
    void grab_focus(uint32_t ch);
    bool handle_input(uint32_t ch);
    void redraw();
    void cleanup();
};

#endif
