#ifndef _DEBUGGER_FRONTEND_H
#define _DEBUGGER_FRONTEND_H

#include <stdint.h>
#include <string>

static const std::string help =
    "Debugger help:\n"
    "? - Display the help text\n"
    "w - Write to one of the supported locations\n"
    "d - Add/remove a breakpoint, or continue debugging.\n"
    "p - Dump supported values\n"
    "q - Exit this command line.\n";

static const std::string dump_help =
    "pb - Dump a list of Breakpoints currently set\n"
    "ps - Dump the Stack\n"
    "pp - Print the Program counter\n"
    "pf - Print the Frame pointer\n"
    "pt - Print the sTack pointer\n"
    "pc - Print the loaded Code\n"
    "py - Display the symbol table\n";

static const std::string write_help =
    "wp - Write the Program counter\n"
    "wf - Write the Frame pointer\n"
    "wt - Write the sTack pointer\n"
    "ws - Write to the stack directly\n"
    "wc - Write instructions to the loaded code segment\n";

static const std::string debug_help =
    "db - Add a breakpoint\n"
    "dB - Remove a breakpoint\n"
    "dc - Continue to the next breakpoint\n"
    "ds - Step one instruction ahead in the program\n";

enum class DFStatus : uint8_t
{
    NONE = 0,
    EXIT,
    SWITCH_TO_CLI,
    SWITCH_TO_VISUAL
};

class DebuggerFrontend
{
public:
    DebuggerFrontend()
    {
    }

    virtual ~DebuggerFrontend()
    {
    }

    virtual DFStatus run() = 0;

private:
};

#endif
