#include "CLIDebugger.hpp"
#include "Debugger.hpp"
#include "Util.hpp"
#include "Vm.hpp"
#include <algorithm>
#include <cstdio>
#include <cstring>
#include <functional>
#include <iostream>
#include <map>
#include <string>

static constexpr int LINE_WIDTH = 20;
static constexpr size_t STACK_HUD_WIDTH = 16;

static const char code_write_help[] =
    "w c <offset> <instruction>\n"
    "<instruction> must be a single hexadecimal byte\n";
static const char stack_write_help[] = "w s <offset> <byte>\n";

CLIDebugger::CLIDebugger()
{
}

bool CLIDebugger::print_help(db_command argv)
{
    printf("Command: %s\n", argv[0].c_str());
    printf("%s", help.c_str());
    return true;
}

bool CLIDebugger::write(db_command cmd)
{
    if (cmd[0].length() < 2 || cmd.size() < 2)
    {
        printf("%s", write_help.c_str());
        return true;
    }
    int value = cmd[0].c_str()[1];
    switch (value)
    {
    case '?':
    {
        printf("%s\n", write_help.c_str());
        break;
    }
    case 'p':
    {
        int new_pc = to_integer(cmd[1]);
        Debugger::get_instance().set_program_counter(new_pc);
        break;
    }
    case 'f':
    {
        int new_fp = to_integer(cmd[1]);
        Debugger::get_instance().set_frame_pointer(new_fp);
        break;
    }
    case 't':
    {
        int new_sp = to_integer(cmd[1]);
        Debugger::get_instance().set_stack_pointer(new_sp);
        break;
    }
    case 's':
    {
        if (cmd.size() < 3)
        {
            printf(stack_write_help);
            return true;
        }
        stack_write(cmd);
        break;
    }
    case 'c':
    {
        if (cmd.size() < 3)
        {
            printf(code_write_help);
            return true;
        }
        code_write(cmd);
        break;
    }
    default:
    {
        printf("Unrecognized option %c. Valid options are:\n%s", value,
               write_help.c_str());
        break;
    }
    }
    return true;
}

void CLIDebugger::code_write(db_command cmd)
{
    if (is_integer(cmd[1]) && is_integer(cmd[2]))
    {
        if (!Debugger::get_instance().code_write(to_integer(cmd[1]),
                                                 to_integer(cmd[2])))
        {
            printf("Code segment is only %ld bytes\n",
                   Debugger::get_instance().get_code_segment_size());
        }
    }
    else
    {
        printf(code_write_help);
    }
}

void CLIDebugger::stack_write(db_command cmd)
{
    if (is_integer(cmd[1]) && is_integer(cmd[2]))
    {
        if (!Debugger::get_instance().code_write(to_integer(cmd[1]),
                                                 to_integer(cmd[2])))
        {
            printf("Cannot write to a region off the stack\n");
        }
    }
    else
    {
        printf(stack_write_help);
    }
}

bool CLIDebugger::dump(db_command argv)
{
    if (argv[0].length() < 2 || argv.size() < 1)
    {
        printf("%s", dump_help.c_str());
        return true;
    }
    char loc = argv[0].c_str()[1];
    switch (loc)
    {
    case '?':
    {
        printf("%s", dump_help.c_str());
        break;
    }
    case 's':
    {
        size_t print_len = Debugger::get_instance().get_stack_pointer();
        if (print_len == 0)
        {
            print_len = STACK_HUD_WIDTH;
        }
        else if (print_len % STACK_HUD_WIDTH != 0)
        {
            print_len += print_len % STACK_HUD_WIDTH;
        }
        print_top_stack(print_len);
        break;
    }
    case 'b':
    {
        for (auto it = Debugger::get_instance().list_breakpoints().begin();
             it != Debugger::get_instance().list_breakpoints().end(); ++it)
        {
            printf("bp at %x\n", *it);
        }
        break;
    }
    case 'p':
    {
        printf("Program Counter: 0x%x\n",
               Debugger::get_instance().get_program_counter());
        break;
    }
    case 'f':
    {
        printf("Frame Pointer: 0x%x\n",
               Debugger::get_instance().get_frame_pointer());
        break;
    }
    case 't':
    {
        printf("Stack Pointer: 0x%x\n",
               Debugger::get_instance().get_stack_pointer());
        break;
    }
    case 'c':
    {
        printf("Program Code: (0x%lx op codes)\n",
               Debugger::get_instance().get_code_segment_size());
        int wrapper = 0;
        for (size_t i = 0; i < Debugger::get_instance().get_code_segment_size();
             i++)
        {
            if (wrapper % LINE_WIDTH == 0)
            {
                printf("\n");
            }
            printf("%02X ", *(Debugger::get_instance().get_code_segment() + i));
            wrapper++;
        }
        printf("\n");
        break;
    }
    case 'y':
    {
        for (auto it = Debugger::get_instance().symbol_table.begin();
             it != Debugger::get_instance().symbol_table.end(); ++it)
        {
            printf("[%05lx] %s\n", it->first, it->second.c_str());
        }
        break;
    }
    default:
        printf("Unrecognized argument: %c\n%s", loc, dump_help.c_str());
    }
    return true;
}

bool CLIDebugger::debug(db_command argv)
{
    if (argv[0].length() < 2 || argv.size() < 1)
    {
        printf("%s", debug_help.c_str());
        return true;
    }
    char cmd = argv[0].c_str()[1];
    switch (cmd)
    {
    case '?':
    {
        printf("%s", debug_help.c_str());
        break;
    }
    case 'b':
    {
        if (argv.size() < 2)
        {
            printf("db <index>\n");
        }
        else
        {
            int bp = to_integer(argv[1]);
            Debugger::get_instance().set_breakpoint(bp);
            printf("Set breakpoint at 0x%x\n", bp);
        }
        break;
    }
    case 'B':
    {
        if (argv.size() < 2)
        {
            printf("dB <index>\n");
        }
        else
        {
            int bp = to_integer(argv[1]);
            if (Debugger::get_instance().remove_breakpoint(bp))
            {
                printf("Removed breakpoint at 0x%x\n", bp);
            }
            else
            {
                printf("No breakpoint at 0x%x\n", bp);
            }
        }
        break;
    }
    case 'c':
    {
        return Debugger::get_instance().dbg_continue();
    }
    case 's':
    {
        return Debugger::get_instance().step_instruction();
    }
    }

    return true;
}

void CLIDebugger::print_top_stack(size_t len)
{
    printf("-----\n");
    size_t print_upper_bound;
    size_t print_lower_bound;
    if ((int)(Debugger::get_instance().get_stack_pointer() - len) > 0)
    {
        print_upper_bound = Debugger::get_instance().get_stack_pointer();
        print_lower_bound = Debugger::get_instance().get_stack_pointer() - len;
    }
    else
    {
        print_lower_bound = 0;
        print_upper_bound = len;
    }

    printf("\n");
    printf("-offset- ");
    for (size_t i = 0; i < STACK_HUD_WIDTH; i++)
    {
        printf(" %lX ", i);
    }
    printf("    ");
    for (size_t i = 0; i < STACK_HUD_WIDTH; i++)
    {
        printf("%lX", i);
    }
    printf("\n");
    for (size_t i = print_upper_bound; i >= print_lower_bound + STACK_HUD_WIDTH;
         i -= STACK_HUD_WIDTH)
    {
        printf("[%06lx] ", i - STACK_HUD_WIDTH);
        for (size_t x = (i - STACK_HUD_WIDTH); x < i; x++)
        {
            printf("%02X ", Debugger::get_instance().get_stack_element(x));
        }
        printf("    ");
        for (size_t x = (i - STACK_HUD_WIDTH); x < i; x++)
        {
            if (isprint(Debugger::get_instance().get_stack_element(x)))
            {
                printf("%c", Debugger::get_instance().get_stack_element(x));
            }
            else
            {
                printf(".");
            }
        }
        printf("\n");
    }
    printf("\n-----\n");
}

bool CLIDebugger::analyze(db_command argv)
{
    if (argv.size() != 1)
    {
        printf("a accepts one argument\n");
    }

    printf("-----\nProgram Counter: 0x%x\t Stack Pointer: 0x%x\t"
           "Frame Pointer: 0x%x\n",
           Debugger::get_instance().get_program_counter(),
           Debugger::get_instance().get_stack_pointer(),
           Debugger::get_instance().get_frame_pointer());
    print_top_stack(64);

    size_t i = 0;
    while (i < Debugger::get_instance().get_code_segment_size())
    {
        Instruction instr = static_cast<Instruction>(
            Debugger::get_instance().get_code_segment()[i]);

        auto sym = Debugger::get_instance().symbol_table.find(i);
        if (sym != Debugger::get_instance().symbol_table.end())
        {
            printf("%s:\n", sym->second.c_str());
        }

        char breakpoint = ' ';
        if (std::find(Debugger::get_instance().list_breakpoints().begin(),
                      Debugger::get_instance().list_breakpoints().end(),
                      i) != Debugger::get_instance().list_breakpoints().end())
        {
            breakpoint = 'b';
        }

        std::string repr = Debugger::get_instance().get_instruction_name(
            static_cast<int>(instr));
        printf("[%05lx] %c %s ", i, breakpoint, repr.c_str());
        if (instr == Instruction::PUSH)
        {
            i++;
            printf("%d ", Debugger::get_instance().get_code_segment()[i]);
            switch (static_cast<Type>(
                Debugger::get_instance().get_code_segment()[i]))
            {
            case Type::BYTE:
            {
                i++;
                printf("%d", Debugger::get_instance().get_code_segment()[i]);
                break;
            }
            case Type::INT:
            {
                int offset = copy_code_integer(i);
                printf("%d", offset);
                break;
            }
            case Type::FLOAT:
            {
                float offset = copy_code_integer(i);
                printf("%f", offset);
                break;
            }
            }
        }
        if (instr == Instruction::JMP || instr == Instruction::JMPT ||
            instr == Instruction::JMPF || instr == Instruction::ADDR ||
            instr == Instruction::THRD)
        {
            display_symbol(i);
            printf("\n");
        }
        else if (instr == Instruction::LDARG)
        {
            int offset = copy_code_integer(i);
            printf("%d\n", offset);
        }
        else if (instr == Instruction::CALL)
        {
            display_symbol(i);
            int arguments = copy_code_integer(i);
            printf(" %d\n", arguments);
        }
        else if (std::find(Vm::typed_instructions().begin(),
                           Vm::typed_instructions().end(),
                           instr) != Vm::typed_instructions().end())
        {
            i++;
            printf("%d\n", Debugger::get_instance().get_code_segment()[i]);
        }
        else
        {
            printf("\n");
        }
        i++;
    }

    return true;
}

void CLIDebugger::display_symbol(size_t& prog_offset)
{
    int destination = copy_code_integer(prog_offset);
    auto symbol = Debugger::get_instance().symbol_table.find(destination);
    if (symbol != Debugger::get_instance().symbol_table.end())
    {
        printf("sym.%s", symbol->second.c_str());
    }
}

bool CLIDebugger::terminate(db_command argv)
{
    if (argv.size() > 1)
    {
        printf("'q' takes no args\n");
    }
    exit(1);
}

bool CLIDebugger::visual(db_command argv)
{
    if (argv.size() > 1)
    {
        printf("'v' takes no args\n");
    }

    return false;
}

bool CLIDebugger::run_command(db_command cmd)
{
    std::string name(1, cmd[0][0]);
    auto cmd_func = args.find(name);
    bool ret = true;
    if (cmd_func != args.end())
    {
        ret = (cmd_func->second)(cmd);
    }
    else
    {
        printf("Unrecognized command: %s\n", cmd[0].c_str());
    }
    return ret;
}

DFStatus CLIDebugger::run()
{
    printf("Debugger CLI. Type '?' for help.\n");
    printf("Stopped at program counter: %x, stack pointer: %x, frame pointer: "
           "%x\n",
           Debugger::get_instance().get_program_counter(),
           Debugger::get_instance().get_stack_pointer(),
           Debugger::get_instance().get_frame_pointer());
    bool keep_debugging = true;
    do
    {
        printf("> ");
        std::string line;
        std::getline(std::cin, line);

        if (!line.empty())
        {
            std::vector<std::string> argv = tokenizer(line, ' ');
            keep_debugging = run_command(argv);
        }
    } while (keep_debugging);

    return DFStatus::SWITCH_TO_VISUAL;
}
