#ifndef _DEBUGGER_H
#define _DEBUGGER_H

#include "DebuggerFrontend.hpp"
#include "Vm.hpp"
#include <map>
#include <memory>
#include <stdint.h>
#include <string>
#include <vector>

class Vm;
using db_command = std::vector<std::string>&;

class Debugger
{
public:
    std::map<size_t, std::string> symbol_table;

    static Debugger& get_instance();
    Debugger(const Debugger&) = delete;
    Debugger& operator=(const Debugger&) = delete;

    void run();

    void init(Vm* vm, const std::map<std::string, size_t>& symbols);
    bool advance(int pc);

    void set_breakpoint(int bp);
    const std::vector<int>& list_breakpoints() const;
    bool remove_breakpoint(int bp);

    bool dbg_continue();
    bool step_instruction();

    void set_program_counter(int pc);
    void set_stack_pointer(int sp);
    void set_frame_pointer(int fp);
    void set_output_fd(int fd);

    int get_program_counter() const;
    int get_stack_pointer() const;
    int get_frame_pointer() const;

    bool code_write(size_t offset, uint8_t instruction);
    size_t get_code_segment_size() const;
    uint8_t* get_code_segment();
    void set_opcode(size_t offt, uint8_t opcode);

    bool stack_write(size_t stack_location, uint8_t val);
    void set_stack_element(size_t offset, uint8_t element);
    uint8_t get_stack_element(size_t offset) const;

    std::string get_instruction_name(int instr) const;

private:
    Vm* vm;
    std::vector<int> breakpoints;
    std::unique_ptr<DebuggerFrontend> ui;

    Debugger();
};

#endif
