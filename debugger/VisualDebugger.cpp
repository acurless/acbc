#include "VisualDebugger.hpp"
#include "Debugger.hpp"
#include "Util.hpp"
#include <fcntl.h>
#include <memory>
#include <regex>
#include <unistd.h>

static constexpr int LINE_WIDTH = 20;

static constexpr int CHAR_NULL = 0x00;
static constexpr int CHAR_ESCAPE = 0x1B;
static constexpr int CHAR_ENTER = 10;
static constexpr int CHAR_BACKSPACE = 127;
static constexpr int CHAR_COLON = 58;
static constexpr int CHAR_G = 0x67;
static constexpr int CHAR_G_CAPS = 0x47;
static constexpr int CHAR_J = 0x6A;
static constexpr int CHAR_K = 0x6B;
static constexpr int CHAR_UP_ARROW = 0x001B5B41;
static constexpr int CHAR_DOWN_ARROW = 0x001B5B42;
static constexpr int CHAR_RIGHT_ARROW = 0x001B5B43;
static constexpr int CHAR_LEFT_ARROW = 0x001B5B44;

static constexpr char UTF8_HORIZONTAL_BAR[] = "\xe2\x94\x80";
static constexpr char UTF8_VERTICAL_BAR[] = "\xe2\x94\x82";
static constexpr char UTF8_VERTICAL_DOTTED_BAR[] = "\xe2\x95\x8e";
static constexpr char UTF8_TOP_LEFT_CORNER[] = "\xe2\x94\x8c";
static constexpr char UTF8_BOTTOM_LEFT_CORNER[] = "\xe2\x94\x94";

static std::map<Color, Color> inverse_colors = {
    {Color::BLACK, Color::BLACK_INV},   {Color::RED, Color::RED_INV},
    {Color::GREEN, Color::GREEN_INV},   {Color::BLUE, Color::BLUE_INV},
    {Color::CYAN, Color::CYAN_INV},     {Color::MAGENTA, Color::MAGENTA_INV},
    {Color::YELLOW, Color::YELLOW_INV}, {Color::WHITE, Color::WHITE_INV},
    {Color::BLACK_INV, Color::BLACK},   {Color::RED_INV, Color::RED},
    {Color::GREEN_INV, Color::GREEN},   {Color::BLUE_INV, Color::BLUE},
    {Color::CYAN_INV, Color::CYAN},     {Color::MAGENTA_INV, Color::MAGENTA},
    {Color::YELLOW_INV, Color::YELLOW}, {Color::WHITE_INV, Color::WHITE}};

VisualDebugger::VisualDebugger()
{
    setlocale(LC_ALL, "");
    initscr();
    cbreak();
    noecho();
    clear();
    start_color();

    if (pipe(out_pipe) < 0)
    {
        out_pipe[0] = 0;
        out_pipe[1] = 1;
    }
    else
    {
        int flags = fcntl(out_pipe[0], F_GETFL, 0);
        fcntl(out_pipe[0], F_SETFL, flags | O_NONBLOCK);
    }
    Debugger::get_instance().set_output_fd(out_pipe[1]);

    register_elements();
}

VisualDebugger::~VisualDebugger()
{
    Debugger::get_instance().set_output_fd(1);
    close(out_pipe[0]);
    close(out_pipe[1]);

    for (auto& elt : elements)
    {
        delete (elt);
    }
}

void VisualDebugger::register_elements()
{
    iview = new VDInstructionView(10);
    elements.push_back(iview);
    cmdbar = new VDCommandBar(out_pipe[0]);
    elements.push_back(cmdbar);

    std::sort(elements.begin(), elements.end(), [](VDElement* a, VDElement* b) {
        return a->get_priority() > b->get_priority();
    });
}

uint8_t VisualDebugger::read_input()
{
    return getch();
}

uint32_t VisualDebugger::handle_sequence_char(uint32_t ch)
{
    if (ch != CHAR_ESCAPE)
    {
        return ch;
    }

    uint32_t sequence = 0;
    sequence += (ch << 16);

    nodelay(stdscr, true);
    for (int i = 1; i >= 0; i--)
    {
        uint32_t ret = getch();
        if ((int32_t)ret == ERR)
        {
            nodelay(stdscr, false);
            return ch;
        }

        sequence += (ret << (8 * i));
    }
    nodelay(stdscr, false);

    return sequence;
}

void VisualDebugger::grab_focus(uint32_t ch)
{
    (void)ch;
    if (cmdbar->is_visible())
    {
        focus = cmdbar;
    }
    else
    {
        focus = iview;
    }
}

bool VisualDebugger::handle_input(uint32_t ch)
{
    grab_focus(ch);

    if (cmdbar->is_visible())
    {
        return true;
    }

    switch (ch)
    {
    case 'q':
        return false;
    default:
        return true;
    }
}

void VisualDebugger::redraw()
{
    erase();
    for (auto& element : elements)
    {
        if (element->is_visible())
        {
            element->draw();
        }
    }
    refresh();
}

void VisualDebugger::cleanup()
{
    nocbreak();
    echo();
    erase();
    endwin();
}

DFStatus VisualDebugger::run()
{
    bool keep_debugging = true;
    redraw();
    do
    {
        uint32_t ch = read_input();
        ch = handle_sequence_char(ch);

        for (auto& e : elements)
        {
            e->wake(ch);
        }

        if (!handle_input(ch))
        {
            cleanup();
            keep_debugging = false;
        }

        if (focus->handle_input(ch) && keep_debugging)
        {
            redraw();
        }
    } while (keep_debugging);

    return DFStatus::SWITCH_TO_CLI;
}

VDElement::VDElement(int _x, int _y, bool _visible, int _priority)
    : x(_x), y(_y), visible(_visible), priority(_priority)
{
    init_pair((int16_t)Color::BLACK, COLOR_BLACK, COLOR_WHITE);
    init_pair((int16_t)Color::RED, COLOR_RED, COLOR_BLACK);
    init_pair((int16_t)Color::GREEN, COLOR_GREEN, COLOR_BLACK);
    init_pair((int16_t)Color::YELLOW, COLOR_YELLOW, COLOR_BLACK);
    init_pair((int16_t)Color::BLUE, COLOR_BLUE, COLOR_BLACK);
    init_pair((int16_t)Color::MAGENTA, COLOR_MAGENTA, COLOR_BLACK);
    init_pair((int16_t)Color::CYAN, COLOR_CYAN, COLOR_BLACK);
    init_pair((int16_t)Color::WHITE, COLOR_WHITE, COLOR_BLACK);
    init_pair((int16_t)Color::BLACK_INV, COLOR_WHITE, COLOR_BLACK);
    init_pair((int16_t)Color::RED_INV, COLOR_BLACK, COLOR_RED);
    init_pair((int16_t)Color::GREEN_INV, COLOR_BLACK, COLOR_GREEN);
    init_pair((int16_t)Color::YELLOW_INV, COLOR_BLACK, COLOR_YELLOW);
    init_pair((int16_t)Color::BLUE_INV, COLOR_BLACK, COLOR_BLUE);
    init_pair((int16_t)Color::MAGENTA_INV, COLOR_BLACK, COLOR_MAGENTA);
    init_pair((int16_t)Color::CYAN_INV, COLOR_BLACK, COLOR_CYAN);
    init_pair((int16_t)Color::WHITE_INV, COLOR_BLACK, COLOR_WHITE);
}

uint32_t VDElement::wake(uint32_t ch)
{
    (void)ch;
    return ch;
}

bool VDElement::handle_input(uint32_t ch)
{
    (void)ch;
    return false;
}

bool VDElement::is_visible()
{
    return visible;
}

int VDElement::get_priority()
{
    return priority;
}

int VDElement::print_help(db_command cmd)
{
    (void)cmd;
    return COMMAND_SUCCESS;
}

int VDElement::write(db_command cmd)
{
    (void)cmd;
    return COMMAND_SUCCESS;
}

int VDElement::dump(db_command argv)
{
    (void)argv;
    return COMMAND_SUCCESS;
}

int VDElement::debug(db_command cmd)
{
    (void)cmd;
    return COMMAND_SUCCESS;
}

int VDElement::close(db_command cmd)
{
    (void)cmd;
    return COMMAND_SUCCESS;
}

template <typename... Args>
std::string VDElement::write_to_string(const char* format, Args&&... args)
{
    size_t size = snprintf(nullptr, 0, format, std::forward<Args>(args)...) + 1;
    std::unique_ptr<char[]> b = std::make_unique<char[]>(size);

    snprintf(b.get(), size, format, std::forward<Args>(args)...);
    return std::string(b.get(), b.get() + size - 1);
}

std::vector<std::string> VDElement::find_valid_tokens(std::string& line)
{
    std::regex re("[$][0-9A-Za-z/]*[$]");
    std::vector<std::string> matches = regex_searcher(line, re);
    std::vector<std::string> tokens(matches.size());

    auto it =
        std::copy_if(matches.begin(), matches.end(), tokens.begin(),
                     [this](std::string tok) {
                         return formatters.find(tok) != formatters.end() ||
                                colors.find(tok) != colors.end();
                     });
    tokens.resize(std::distance(tokens.begin(), it));

    return tokens;
}

std::vector<colorstring> VDElement::resolve_colors(std::string& line)
{
    std::vector<colorstring> ret;

    line = "$white$" + line + "$white$";
    std::vector<std::string> tokens = find_valid_tokens(line);

    highlight_text = false;
    Color color = Color::WHITE;

    std::vector<std::string>::iterator it;
    for (it = tokens.begin(); it != tokens.end() - 1; it++)
    {
        size_t first = line.find(*it) + it->length();
        size_t last = line.substr(first).find(*(it + 1));

        auto formatter = formatters.find(*it);
        if (formatter != formatters.end())
        {
            (formatter->second)();
        }
        else
        {
            color = colors.find(*it)->second;
        }

        Color c = highlight_text ? invert_color(color) : color;
        colorstring s(line.substr(first, last), c);
        ret.push_back(s);
        line = line.substr(first).substr(last);
    }

    return ret;
}

int VDElement::get_length(std::string s)
{
    int l = s.length();
    l -= std::count(s.begin(), s.end(), '\xe2') * 2;
    return l;
}

Color VDElement::invert_color(Color c)
{
    auto i = inverse_colors.find(c);
    if (i != inverse_colors.end())
    {
        return i->second;
    }

    return Color::BLACK;
}

int VDElement::run_command(db_command cmd)
{
    std::string name(1, cmd[0][0]);
    auto cmd_func = args.find(name);
    if (cmd_func != args.end())
    {
        return (cmd_func->second)(cmd);
    }
    else
    {
        return COMMAND_UNKNOWN;
    }
}

VDCommandBar::VDCommandBar(int& _fd)
    : VDElement(0, LINES - 1, false, 0), history_idx(HISTORY_BEGIN), fd(_fd),
      ignore_ch(false)
{
}

uint32_t VDCommandBar::wake(uint32_t ch)
{
    if (ch == CHAR_COLON)
    {
        visible = true;
        ignore_ch = true;
        return 0;
    }

    return ch;
}

bool VDCommandBar::handle_input(uint32_t ch)
{
    if (!visible)
    {
        return false;
    }
    if (ignore_ch)
    {
        ignore_ch = false;
        return true;
    }

    switch (ch)
    {
    case KEY_BACKSPACE:
    case KEY_DC:
    case CHAR_BACKSPACE:
    {
        buf = buf.substr(0, buf.size() - 1);
        break;
    }
    case KEY_ENTER:
    case CHAR_ENTER:
    {
        handle_command();
        break;
    }
    case CHAR_UP_ARROW:
    {
        last_history(buf);
        break;
    }
    case CHAR_DOWN_ARROW:
    {
        next_history(buf);
        break;
    }
    case CHAR_ESCAPE:
    case 0:
        break;
    default:
        buf += ch;
    }

    return true;
}

bool VDCommandBar::last_history(std::string& buf)
{
    if (!history.size() || history_idx == 0)
    {
        return false;
    }
    if (history_idx == HISTORY_BEGIN)
    {
        history_idx = history.size();
    }

    if (history_idx == history.size())
    {
        tmp_buf = buf;
    }

    history_idx -= 1;
    buf = history[history_idx];

    return true;
}

bool VDCommandBar::next_history(std::string& buf)
{
    if (!history.size() || history_idx + 1 > history.size())
    {
        return false;
    }
    if (history_idx == HISTORY_BEGIN)
    {
        history_idx = history.size();
    }

    history_idx += 1;
    if (history_idx == history.size())
    {
        buf = tmp_buf;
    }
    else
    {
        buf = history[history_idx];
    }

    return true;
}

int VDCommandBar::write(db_command cmd)
{
    if (!cmd.size() || cmd[0].length() < 2)
    {
        log(write_help, NULL);
        return COMMAND_SUCCESS;
    }

    int value = cmd[0].c_str()[1];
    switch (value)
    {
    case '?':
    {
        log(write_help, NULL);
        break;
    }
    case 'p':
    {
        if (cmd.size() < 2)
        {
            log("wp <value>\n", NULL);
            return COMMAND_SUCCESS;
        }
        int new_pc = to_integer(cmd[1]);
        Debugger::get_instance().set_program_counter(new_pc);
        break;
    }
    case 'f':
    {
        if (cmd.size() < 2)
        {
            log("wf <value>\n", NULL);
            return COMMAND_SUCCESS;
        }
        int new_fp = to_integer(cmd[1]);
        Debugger::get_instance().set_frame_pointer(new_fp);
        break;
    }
    case 't':
    {
        if (cmd.size() < 2)
        {
            log("wt <value>\n", NULL);
            return COMMAND_SUCCESS;
        }
        int new_sp = to_integer(cmd[1]);
        Debugger::get_instance().set_stack_pointer(new_sp);
        break;
    }
    case 's':
    {
        if (cmd.size() < 3)
        {
            log("ws <offset> <byte>\n", NULL);
            return COMMAND_SUCCESS;
        }
        stack_write(cmd);
        break;
    }
    case 'c':
    {
        if (cmd.size() < 3)
        {
            log("wc <offset> <instruction>\n", NULL);
            return COMMAND_SUCCESS;
        }
        code_write(cmd);
        break;
    }
    default:
    {
        log("Unrecognized option %c. Valid options are:\n%s", value,
            write_help.c_str());
        break;
    }
    }

    return COMMAND_SUCCESS;
}

void VDCommandBar::code_write(db_command cmd)
{
    if (is_integer(cmd[1]) && is_integer(cmd[2]))
    {
        if (!Debugger::get_instance().code_write(to_integer(cmd[1]),
                                                 to_integer(cmd[2])))
        {
            log("Code segment is only %ld bytes\n",
                Debugger::get_instance().get_code_segment_size());
        }
    }
    else
    {
        log("wc <offset> <instruction>\n", NULL);
    }
}

void VDCommandBar::stack_write(db_command cmd)
{
    if (is_integer(cmd[1]) && is_integer(cmd[2]))
    {
        if (!Debugger::get_instance().stack_write(to_integer(cmd[1]),
                                                  to_integer(cmd[2])))
        {
            log("Cannot write to a region off the stack\n", NULL);
        }
    }
    else
    {
        log("ws <offset> <byte>\n", NULL);
    }
}

int VDCommandBar::debug(db_command cmd)
{
    if (!cmd.size() || cmd[0].length() < 2)
    {
        log(debug_help, NULL);
        return COMMAND_SUCCESS;
    }

    int value = cmd[0].c_str()[1];
    switch (value)
    {
    case '?':
    {
        log(debug_help, NULL);
        break;
    }
    case 'b':
    {
        if (cmd.size() < 2)
        {
            log("db <index>\n", NULL);
            return COMMAND_SUCCESS;
        }
        int bp = to_integer(cmd[1]);
        Debugger::get_instance().set_breakpoint(bp);
        log("Set breakpoint at 0x%x\n", bp);
        break;
    }
    case 'B':
    {
        if (cmd.size() < 2)
        {
            log("dB <index>\n", NULL);
            return COMMAND_SUCCESS;
        }
        int bp = to_integer(cmd[1]);
        if (Debugger::get_instance().remove_breakpoint(bp))
        {
            log("Removed breakpoint at 0x%x\n", bp);
        }
        else
        {
            log("No breakpoint at 0x%x\n", bp);
        }
        break;
    }
    case 'c':
    {
        if (Debugger::get_instance().dbg_continue())
        {
            log("Program stopped at address 0x%x\n",
                Debugger::get_instance().get_program_counter());
            return COMMAND_SUCCESS;
        }
        else
        {
            return COMMAND_HALT;
        }
    }
    case 's':
    {
        if (Debugger::get_instance().step_instruction())
        {
            log("Step to address 0x%x\n",
                Debugger::get_instance().get_program_counter());
            return COMMAND_SUCCESS;
        }
        else
        {
            return COMMAND_HALT;
        }
    }
    default:
    {
        log("Unrecognized option %c. Valid options are:\n%s", value,
            write_help.c_str());
        break;
    }
    }

    return COMMAND_SUCCESS;
}

int VDCommandBar::dump(db_command argv)
{
    if (argv[0].length() < 2)
    {
        log(dump_help, NULL);
        return COMMAND_SUCCESS;
    }
    char loc = argv[0].c_str()[1];
    switch (loc)
    {
    case '?':
    {
        log(dump_help, NULL);
        break;
    }
    case 's':
    {
        int length, offset;
        if (argv.size() < 2)
        {
            length = Debugger::get_instance().get_stack_pointer();
        }
        else if (is_integer(argv[2]))
        {
            length = to_integer(argv[2]);
        }
        else
        {
            log("ps <length?> <offset?>\n", NULL);
            return COMMAND_SUCCESS;
        }

        if (argv.size() < 3)
        {
            offset = 0;
        }
        else if (is_integer(argv[3]))
        {
            offset = to_integer(argv[3]);
        }
        else
        {
            log("ps <length?> <offset?>\n", NULL);
            return COMMAND_SUCCESS;
        }

        print_stack(length, offset);
        break;
    }
    case 'b':
    {
        for (auto it = Debugger::get_instance().list_breakpoints().begin();
             it != Debugger::get_instance().list_breakpoints().end(); ++it)
        {
            log("$hl$bp at %x$/hl$\n", *it);
        }
        break;
    }
    case 'p':
    {
        log("Program Counter: 0x%x\n",
            Debugger::get_instance().get_program_counter());
        break;
    }
    case 'f':
    {
        log("Frame Pointer: 0x%x\n",
            Debugger::get_instance().get_frame_pointer());
        break;
    }
    case 't':
    {
        log("Stack Pointer: 0x%x\n",
            Debugger::get_instance().get_stack_pointer());
        break;
    }
    case 'c':
    {
        log("Program Code: (0x%lx op codes)\n",
            Debugger::get_instance().get_code_segment_size());
        int wrapper = 0;
        for (size_t i = 0; i < Debugger::get_instance().get_code_segment_size();
             i++)
        {
            if (wrapper % LINE_WIDTH == 0)
            {
                log("\n", NULL);
            }
            log("%02X ", *(Debugger::get_instance().get_code_segment() + i));
            wrapper++;
        }
        log("\n", NULL);
        break;
    }
    case 'y':
    {
        for (auto it = Debugger::get_instance().symbol_table.begin();
             it != Debugger::get_instance().symbol_table.end(); ++it)
        {
            log("[%05lx] %s\n", it->first, it->second.c_str());
        }
        break;
    }
    default:
        log("Unrecognized argument: %c. Valid options are:\n%s", loc,
            dump_help.c_str());
    }

    return COMMAND_SUCCESS;
}

void VDCommandBar::print_stack(int length, int offset)
{
    int start = Debugger::get_instance().get_stack_pointer() - offset;
    int byte_count;
    int bytes_written = 0;

    while (length)
    {
        std::string line;

        if (length % 4)
        {
            byte_count = length % 4;
        }
        else
        {
            byte_count = 4;
        }

        int address =
            Debugger::get_instance().get_stack_pointer() - bytes_written - 4;

        line += write_to_string("$green$0x%06x", address);
        line += " $white$0x";

        for (int i = byte_count; i < 4; i++)
        {
            line += "00";
        }
        for (int i = 0; i < byte_count; i++)
        {
            uint8_t c =
                Debugger::get_instance().get_stack_element(start - i - 1);
            line += write_to_string("%02X", c);
        }
        line += "$yellow$   ";

        for (int i = byte_count; i < 4; i++)
        {
            line += " ";
        }
        for (int i = 0; i < byte_count; i++)
        {
            uint8_t c =
                Debugger::get_instance().get_stack_element(start - i - 1);
            if (c >= 0x20 && x < 0x7F)
            {
                line += write_to_string("%c", c);
            }
            else
            {
                line += ".";
            }
        }

        log(line, NULL);

        start -= byte_count;
        length -= byte_count;
        bytes_written += byte_count;
    }
}

int VDCommandBar::print_help(db_command cmd)
{
    (void)cmd;
    log(help, NULL);
    return COMMAND_SUCCESS;
}

int VDCommandBar::close(db_command cmd)
{
    (void)cmd;
    visible = false;
    output.clear();
    buf = "";

    return COMMAND_SUCCESS;
}

template <typename... Args>
void VDCommandBar::log(const std::string& format, Args&&... args)
{
    std::string out =
        write_to_string(format.c_str(), std::forward<Args>(args)...);
    std::vector<std::string> lines = tokenizer(out, '\n');

    for (std::string& line : lines)
    {
        std::vector<colorstring> parts = resolve_colors(line);
        output.push_back(parts);
    }
}

void VDCommandBar::handle_command()
{
    log(":> " + buf, NULL);

    std::vector<std::string> argv = tokenizer(buf, ' ');

    if (buf.empty())
    {
        close(argv);
        return;
    }

    switch (run_command(argv))
    {
    case COMMAND_UNKNOWN:
        log("Unrecognized command: %s\n", argv[0].c_str());
        break;
    case COMMAND_HALT:
        log("Program has halted. Reset the debug session to continue.\n", NULL);
        break;
    default:
        break;
    }

    history.push_back(buf);
    history_idx = HISTORY_BEGIN;
    buf = "";
}

void VDCommandBar::read_from_stdout()
{
    std::string o;
    char c;
    while (read(fd, &c, sizeof(char)) > 0)
    {
        o += c;
    }

    std::vector<std::string> tokens = tokenizer(o, '\n');

    for (auto& t : tokens)
    {
        output.push_back(resolve_colors(t));
    }
}

void VDCommandBar::draw()
{
    read_from_stdout();

    y = LINES - 1;
    move(y, 0);
    clrtoeol();
    mvprintw(y, x, ":> %s", buf.c_str());

    std::vector<std::vector<colorstring>>::reverse_iterator rit;
    int _y;

    for (rit = output.rbegin(), _y = y - 1; rit != output.rend() && _y > 0;
         ++rit, --_y)
    {
        move(_y, 0);
        clrtoeol();

        int _x = x;
        for (colorstring& p : *rit)
        {
            attron(COLOR_PAIR(std::get<1>(p)));
            mvprintw(_y, _x, "%s", std::get<0>(p).c_str());
            attroff(COLOR_PAIR(std::get<1>(p)));
            _x += std::get<0>(p).length();
        }
    }

    move(y, x + buf.length() + 3);
}

VDInstructionView::VDInstructionView(int y)
    : VDElement(0, y, true, 1), top_idx(0)
{
}

uint32_t VDInstructionView::wake(uint32_t ch)
{
    visible = true;
    return ch;
}

bool VDInstructionView::handle_input(uint32_t ch)
{
    if (!visible)
    {
        return false;
    }

    switch (ch)
    {
    case CHAR_K:
    case CHAR_UP_ARROW:
    {
        if (top_idx > 0)
        {
            top_idx -= 1;
        }
        break;
    }
    case CHAR_J:
    case CHAR_DOWN_ARROW:
    {
        if (top_idx + (LINES - y) < codes.size())
        {
            top_idx += 1;
        }
        break;
    }
    case CHAR_G_CAPS:
    {
        int new_idx = codes.size() - (LINES - y);
        top_idx = (new_idx < 0) ? 0 : new_idx;
        break;
    }
    case CHAR_G:
    {
        top_idx = 0;
        break;
    }
    default:
        break;
    }

    return true;
}

void VDInstructionView::expand_code(std::string& code)
{
    std::string line = code;
    std::vector<std::string> tokens = find_valid_tokens(code);

    std::vector<std::string>::iterator it;
    for (it = tokens.begin(); it != tokens.end(); it++)
    {
        size_t first = line.find(*it);
        size_t last = line.find(*it) + it->length();
        line = line.substr(0, first) + line.substr(last);
    }
    while (line.length() < MAX_INSTRUCTION_SIZE * 2)
    {
        code += " ";
        line += " ";
    }
}

void VDInstructionView::write_instruction(std::string& code,
                                          std::string& code_raw, size_t& idx)
{
    Instruction instr = static_cast<Instruction>(
        Debugger::get_instance().get_code_segment()[idx]);
    std::string repr =
        Debugger::get_instance().get_instruction_name(static_cast<int>(instr));

    code += write_to_string("$magenta$%s ", repr.c_str());
    code_raw += write_to_string("$magenta$%02x", static_cast<int>(instr));
}

void VDInstructionView::write_type(std::string& code, std::string& code_raw,
                                   size_t& idx)
{
    uint8_t type = Debugger::get_instance().get_code_segment()[idx];
    code_raw += write_to_string("$white$%02x", type);

    switch (static_cast<Type>(type))
    {
    case Type::BYTE:
    {
        code += "$blue$(byte) ";
        break;
    }
    case Type::INT:
    {
        code += "$blue$(int) ";
        break;
    }
    case Type::FLOAT:
    {
        code += "$blue$(float) ";
    }
    }
}

std::string VDInstructionView::write_push(size_t& idx)
{
    std::string code, code_raw, _code;

    write_instruction(code, code_raw, idx);
    idx++;
    write_type(_code, code_raw, idx);

    Type type =
        static_cast<Type>(Debugger::get_instance().get_code_segment()[idx]);

    uint32_t offset;
    switch (type)
    {
    case Type::BYTE:
    {
        idx++;
        offset = Debugger::get_instance().get_code_segment()[idx];
        code_raw += write_to_string("$yellow$%02x", offset);
        break;
    }
    case Type::INT:
    {
        offset = copy_code_integer(idx);
        code_raw += write_to_string("$yellow$%08x", offset);
        break;
    }
    case Type::FLOAT:
    {
        offset = copy_code_integer(idx);
        code_raw += write_to_string("$yellow$%08x", offset);
        break;
    }
    }

    code += write_to_string("$yellow$0x%lx", offset);
    code = code + " " + _code;

    if (type == Type::BYTE && offset >= 0x20 && offset < 0x7F)
    {
        code += write_to_string("$red$  ; %c", static_cast<char>(offset));
    }

    expand_code(code_raw);
    return code_raw + "  " + code;
}

std::string VDInstructionView::write_jmp(size_t& idx)
{
    std::string code, code_raw;

    write_instruction(code, code_raw, idx);

    int destination = copy_code_integer(idx);
    code_raw += write_to_string("$red$%08x", destination);

    auto symbol = Debugger::get_instance().symbol_table.find(destination);
    if (symbol != Debugger::get_instance().symbol_table.end())
    {
        code += write_to_string("$red$sym.%s", symbol->second.c_str());
    }

    expand_code(code_raw);
    return write_to_string("%s  %s", code_raw.c_str(), code.c_str());
}

std::string VDInstructionView::write_ldarg(size_t& idx)
{
    std::string code, code_raw;

    write_instruction(code, code_raw, idx);

    int offset = copy_code_integer(idx);
    code_raw += write_to_string("$yellow$%02x", offset);
    code += write_to_string("$yellow$%d", offset);

    expand_code(code_raw);
    return write_to_string("%s  %s", code_raw.c_str(), code.c_str());
}

std::string VDInstructionView::write_call(size_t& idx)
{
    std::string code, code_raw;

    write_instruction(code, code_raw, idx);

    int destination = copy_code_integer(idx);
    code_raw += write_to_string("$red$%08x", destination);

    auto symbol = Debugger::get_instance().symbol_table.find(destination);
    if (symbol != Debugger::get_instance().symbol_table.end())
    {
        code += write_to_string("$red$%s", symbol->second.c_str());
    }

    int arguments = copy_code_integer(idx);
    code_raw += write_to_string("$yellow$%02x", arguments);
    code += write_to_string("$yellow$ [%d]", arguments);

    expand_code(code_raw);
    return write_to_string("%s  %s", code_raw.c_str(), code.c_str());
}

void VDInstructionView::write_code(size_t& i)
{
    size_t start = i;

    auto sym = Debugger::get_instance().symbol_table.find(i);
    if (sym != Debugger::get_instance().symbol_table.end())
    {
        VDInstruction vdi(0, Instruction::NOP,
                          write_to_string("$red$%s", sym->second.c_str()));
        codes.push_back(vdi);
    }

    std::string line;
    std::string addr_fmt = "$green$0x%08lx";

    if (start == (size_t)Debugger::get_instance().get_program_counter())
    {
        addr_fmt = "$hl$" + addr_fmt + "$/hl$";
    }
    line += write_to_string(addr_fmt.c_str(), i);

    if (std::find(Debugger::get_instance().list_breakpoints().begin(),
                  Debugger::get_instance().list_breakpoints().end(),
                  i) != Debugger::get_instance().list_breakpoints().end())
    {
        line += "$cyan$  *  ";
    }
    else
    {
        line += "     ";
    }

    Instruction instr = static_cast<Instruction>(
        Debugger::get_instance().get_code_segment()[i]);

    auto writer = writers.find(instr);
    if (writer != writers.end())
    {
        line += (writer->second)(i);
    }
    else
    {
        std::string code, code_raw;

        write_instruction(code, code_raw, i);

        if (std::find(Vm::typed_instructions().begin(),
                      Vm::typed_instructions().end(),
                      instr) != Vm::typed_instructions().end())
        {
            i++;
            write_type(code, code_raw, i);
        }

        expand_code(code_raw);
        line += write_to_string("%s  %s", code_raw.c_str(), code.c_str());
    }

    VDInstruction vdi(start, instr, line);
    if (std::find(Vm::jump_instructions().begin(),
                  Vm::jump_instructions().end(),
                  instr) != Vm::jump_instructions().end())
    {
        i -= sizeof(int);
        vdi.jmp_idx = copy_code_integer(i);
    }

    codes.push_back(vdi);
    i++;
}

template <typename T>
void VDInstructionView::update_jump_paths(T s, T e, bool direction)
{
    uint8_t vertical_style =
        direction ? VDInstruction::DOWN : VDInstruction::UP;
    uint8_t enter_corner_style =
        direction ? VDInstruction::TOP_CORNER : VDInstruction::BOTTOM_CORNER;
    uint8_t exit_corner_style =
        direction ? VDInstruction::BOTTOM_CORNER : VDInstruction::TOP_CORNER;
    uint8_t arrow_style =
        direction ? VDInstruction::BOTTOM_ARROW : VDInstruction::TOP_ARROW;

    size_t pos = std::max_element(s, e,
                                  [](VDInstruction& a, VDInstruction& b) {
                                      return a.arrows.size() < b.arrows.size();
                                  })
                     ->arrows.size() +
                 1;

    T it;
    for (it = s; it <= e; it++)
    {
        while (it->arrows.size() + 1 < pos)
        {
            it->arrows.push_back(VDInstruction::EMPTY);
        }
        it->arrows.push_back(vertical_style);
    }

    std::vector<uint8_t>::iterator _it;
    for (_it = s->arrows.begin(); _it < s->arrows.begin() + (pos - 1); _it++)
    {
        *_it = VDInstruction::HORIZONTAL;
    }
    s->arrows[pos - 1] = enter_corner_style;
    for (_it = e->arrows.begin(); _it < e->arrows.begin() + (pos - 1); _it++)
    {
        *_it = VDInstruction::HORIZONTAL;
    }
    e->arrows[pos - 1] = exit_corner_style;
    e->arrows[0] = arrow_style;
}

void VDInstructionView::find_jump_paths()
{
    std::vector<VDInstruction>::iterator s, e;
    std::vector<VDInstruction>::reverse_iterator rs, re;

    std::vector<VDInstruction>::iterator cbegin, cend;
    cbegin = codes.begin() + top_idx;
    cend = codes.begin() + top_idx + (LINES - y);

    for (s = cbegin; s < cend && s != codes.end(); s++)
    {
        if (std::find(Vm::jump_instructions().begin(),
                      Vm::jump_instructions().end(),
                      s->instruction) != Vm::jump_instructions().end())
        {
            rs = make_reverse_iterator(s + 1);

            if (s->jmp_idx > s->idx)
            {
                for (e = s; e->idx != s->jmp_idx && e != codes.end(); e++)
                    ;
                if (e != codes.end())
                {
                    update_jump_paths<std::vector<VDInstruction>::iterator>(
                        s, e, true);
                }
            }
            else
            {
                for (re = rs; re->idx != rs->jmp_idx && re != codes.rend();
                     re++)
                    ;
                if (re != codes.rend())
                {
                    update_jump_paths<
                        std::vector<VDInstruction>::reverse_iterator>(rs, re,
                                                                      false);
                }
            }
        }
    }
}

std::string VDInstructionView::ad_empty(std::vector<uint8_t>& a)
{
    (void)a;
    return "  ";
}

std::string VDInstructionView::ad_down(std::vector<uint8_t>& a)
{
    (void)a;
    return write_to_string("%s ", UTF8_VERTICAL_BAR);
}

std::string VDInstructionView::ad_up(std::vector<uint8_t>& a)
{
    (void)a;
    return write_to_string("%s ", UTF8_VERTICAL_DOTTED_BAR);
}
std::string VDInstructionView::ad_horizontal(std::vector<uint8_t>& a)
{
    (void)a;
    return write_to_string("%s%s", UTF8_HORIZONTAL_BAR, UTF8_HORIZONTAL_BAR);
}

std::string VDInstructionView::ad_top_arrow(std::vector<uint8_t>& a)
{
    if (a.size() <= 1 || a[1] == VDInstruction::EMPTY)
    {
        return write_to_string("%s%c", UTF8_TOP_LEFT_CORNER, '>');
    }
    else
    {
        return write_to_string("%s%c", UTF8_HORIZONTAL_BAR, '>');
    }
}

std::string VDInstructionView::ad_bottom_arrow(std::vector<uint8_t>& a)
{
    if (a.size() == 1 || a[1] == VDInstruction::EMPTY)
    {
        return write_to_string("%s%c", UTF8_BOTTOM_LEFT_CORNER, '>');
    }
    else
    {
        return write_to_string("%s%c", UTF8_HORIZONTAL_BAR, '>');
    }
}

std::string VDInstructionView::ad_top_corner(std::vector<uint8_t>& a)
{
    (void)a;
    return write_to_string("%s%s", UTF8_TOP_LEFT_CORNER, UTF8_HORIZONTAL_BAR);
}

std::string VDInstructionView::ad_bottom_corner(std::vector<uint8_t>& a)
{
    (void)a;
    return write_to_string("%s%s", UTF8_BOTTOM_LEFT_CORNER,
                           UTF8_HORIZONTAL_BAR);
}

std::string VDInstructionView::render_instruction(VDInstruction instr)
{
    std::vector<uint8_t>& a = instr.arrows;
    while (a.size() < MAX_INSTRUCTION_SIZE)
    {
        a.push_back(VDInstruction::EMPTY);
    }

    std::string prefix = write_to_string("$blue$");
    std::vector<uint8_t>::reverse_iterator rit;
    for (rit = a.rbegin();
         rit < a.rbegin() + MAX_INSTRUCTION_SIZE && rit != a.rend(); rit++)
    {
        auto drawer = arrow_drawers.find(*rit);
        prefix += (drawer->second)(a);
    }

    return prefix + instr.line;
}

void VDInstructionView::write_codes()
{
    size_t i = 0;
    while (i < Debugger::get_instance().get_code_segment_size())
    {
        write_code(i);
    }

    find_jump_paths();
}

void VDInstructionView::draw()
{
    codes.clear();
    write_codes();

    int _y;
    std::vector<VDInstruction>::iterator it;
    for (_y = y, it = codes.begin() + top_idx; _y < LINES && it != codes.end();
         _y++, it++)
    {
        int _x = x;
        std::string line_raw = render_instruction(*it);
        std::vector<colorstring> line = resolve_colors(line_raw);
        for (colorstring& p : line)
        {
            attron(COLOR_PAIR(std::get<1>(p)));
            mvprintw(_y, _x, "%s", std::get<0>(p).c_str());
            attroff(COLOR_PAIR(std::get<1>(p)));
            _x += get_length(std::get<0>(p));
        }
    }
}
