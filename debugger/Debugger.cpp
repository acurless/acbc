#include "Debugger.hpp"
#include "CLIDebugger.hpp"
#include "VisualDebugger.hpp"

#include <algorithm>
#include <memory>

static std::map<Instruction, std::string> instruction_mapping;

Debugger& Debugger::get_instance()
{
    static Debugger dbg;
    return dbg;
}

Debugger::Debugger() : ui(std::make_unique<CLIDebugger>())
{
    const std::map<const std::string, Instruction>& names =
        Vm::instruction_names();
    for (auto it = names.cbegin(); it != names.cend(); ++it)
    {
        instruction_mapping.insert({it->second, it->first});
    }
}

void Debugger::init(Vm* vm, const std::map<std::string, size_t>& symbols)
{
    this->vm = vm;

    for (auto it = symbols.cbegin(); it != symbols.cend(); ++it)
    {
        symbol_table.insert({it->second, it->first});
    }
}

bool Debugger::advance(int pc)
{
    return std::find(breakpoints.begin(), breakpoints.end(), pc) ==
           this->breakpoints.end();
}

void Debugger::set_breakpoint(int bp)
{
    this->breakpoints.push_back(bp);
}

const std::vector<int>& Debugger::list_breakpoints() const
{
    return breakpoints;
}

bool Debugger::remove_breakpoint(int bp)
{
    auto bpoint =
        std::find(this->breakpoints.begin(), this->breakpoints.end(), bp);
    if (bpoint != this->breakpoints.end())
    {
        this->breakpoints.erase(bpoint);
        return true;
    }
    else
    {
        return false;
    }
}

bool Debugger::dbg_continue()
{
    step_instruction();
    while (vm->is_running())
    {
        if (!advance(vm->get_program_counter()))
        {
            break;
        }
        vm->execute_one_instruction();
    }

    return vm->is_running();
}

bool Debugger::step_instruction()
{
    if (!vm->is_running())
    {
        return false;
    }
    vm->execute_one_instruction();
    return vm->is_running();
}

void Debugger::set_program_counter(int pc)
{
    this->vm->pc = pc;
}

void Debugger::set_stack_pointer(int sp)
{
    this->vm->sp = sp;
}

void Debugger::set_frame_pointer(int fp)
{
    this->vm->fp = fp;
}

void Debugger::set_output_fd(int fd)
{
    this->vm->fd = fd;
}

int Debugger::get_program_counter() const
{
    return this->vm->pc;
}

int Debugger::get_stack_pointer() const
{
    return this->vm->sp;
}

int Debugger::get_frame_pointer() const
{
    return this->vm->fp;
}

bool Debugger::code_write(size_t offset, uint8_t instruction)
{
    if (offset > get_code_segment_size())
    {
        return false;
    }
    else
    {
        Debugger::get_instance().get_code_segment()[offset] = instruction;
        return true;
    }
}

uint8_t* Debugger::get_code_segment()
{
    return &this->vm->code[0];
}

void Debugger::set_opcode(size_t offt, uint8_t opcode)
{
    this->vm->code[offt] = opcode;
}

size_t Debugger::get_code_segment_size() const
{
    return this->vm->code.size();
}

bool Debugger::stack_write(size_t stack_location, uint8_t val)
{
    if (stack_location > Vm::STACK_SIZE)
    {
        return false;
    }
    else
    {
        Debugger::get_instance().set_stack_element(stack_location, val);
        return true;
    }
}

void Debugger::set_stack_element(size_t offset, uint8_t element)
{
    this->vm->vmem->set_byte(offset, element);
}

uint8_t Debugger::get_stack_element(size_t offset) const
{
    return this->vm->vmem->at(offset);
}

std::string Debugger::get_instruction_name(int instr) const
{
    return instruction_mapping[static_cast<Instruction>(instr)];
}

void Debugger::run()
{
    while (true)
    {
        DFStatus ret = this->ui->run();
        switch (ret)
        {
        case DFStatus::SWITCH_TO_CLI:
            this->ui = std::make_unique<CLIDebugger>();
            break;
        case DFStatus::SWITCH_TO_VISUAL:
            this->ui = std::make_unique<VisualDebugger>();
            break;
        case DFStatus::EXIT:
            return;
        default:
            break;
        }
    }
}
