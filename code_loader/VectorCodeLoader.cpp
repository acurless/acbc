#include "VectorCodeLoader.hpp"

VectorCodeLoader::VectorCodeLoader()
{
}

VectorCodeLoader::~VectorCodeLoader()
{
}

const uint8_t& VectorCodeLoader::operator[](const size_t& offset) const
{
    return this->code[offset];
}

uint8_t& VectorCodeLoader::operator[](const size_t& offset)
{
    return this->code[offset];
}

void VectorCodeLoader::push_back(const uint8_t& offset)
{
    this->code.push_back(offset);
}

size_t VectorCodeLoader::size() const
{
    return this->code.size();
}

std::vector<uint8_t>::iterator VectorCodeLoader::begin() noexcept
{
    return this->code.begin();
}
std::vector<uint8_t>::const_iterator VectorCodeLoader::end() const noexcept
{
    return this->code.end();
}
