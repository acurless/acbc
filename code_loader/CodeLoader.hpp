#ifndef _CODE_LOADER_H
#define _CODE_LOADER_H

#include <cstdint>
#include <cstdlib>
#include <vector>

class CodeLoader
{
public:
    CodeLoader()
    {
    }

    virtual ~CodeLoader()
    {
    }

    virtual const uint8_t& operator[](const size_t& offset) const = 0;
    virtual uint8_t& operator[](const size_t& offset) = 0;

    virtual void push_back(const uint8_t& offset) = 0;

    virtual size_t size() const = 0;

    virtual std::vector<uint8_t>::iterator begin() noexcept = 0;
    virtual std::vector<uint8_t>::const_iterator end() const noexcept = 0;
};

#endif
