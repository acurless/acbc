#ifndef _VECTOR_CODE_LOADER_H
#define _VECTOR_CODE_LOADER_H

#include "CodeLoader.hpp"
#include <vector>

class VectorCodeLoader : public CodeLoader
{
public:
    VectorCodeLoader();
    ~VectorCodeLoader();

    const uint8_t& operator[](const size_t& offset) const override;
    virtual uint8_t& operator[](const size_t& offset) override;
    void push_back(const uint8_t& offset) override;

    size_t size() const override;

    std::vector<uint8_t>::iterator begin() noexcept override;
    std::vector<uint8_t>::const_iterator end() const noexcept override;

private:
    std::vector<uint8_t> code;
};

#endif
