"
" Vim Syntax file
" Language: acbc
" Maintainer: Adrian Curless
" Latest Revision: 7 January 2020
"

" Arithmetic instructions
syn keyword acbcInstruction ADD SUB MUL MOD
" Bitwise instructions
syn keyword acbcInstruction AND OR RTL RTR XOR
" Comparison instructions
syn keyword acbcInstruction EQ LT GT
" Control flow instructions
syn keyword acbcInstruction ASSRT CALL HALT JMP JMPT JMPF RET
" Stack instructions
syn keyword acbcInstruction ADDR COPY LD LDARG LDI POP PUSH STR STRI
" Output instructions
syn keyword acbcInstruction GETC PRNT PUTC
" Multithreading instructions
syn keyword acbcInstruction THRD JOIN
" Heap instructions
syn keyword acbcInstruction HSTRT
" Data types
syn keyword acbcType I B F

syn match acbcNumber '\d\+'
syn match acbcNumber '[-+]\d\+'
syn match acbcComment "#.*$"
syn match acbcSymbol 'sym.+'
syn match acbcMarker '[a-z_]+:'

let b:current_syntax = 'acbc'
hi def link acbcInstruction Statement
hi def link acbcNumber      Constant
hi def link acbcComment     Comment
hi def link acbcSymbol      Function
hi def link acbcMarker      Function
hi def link acbcType        Type
