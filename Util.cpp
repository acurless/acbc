#include "Util.hpp"
#include "debugger/Debugger.hpp"

#include <cstdlib>
#include <iostream>
#include <regex>
#include <sstream>

std::vector<std::string> tokenizer(const std::string& input, char delim)
{
    std::vector<std::string> tokens;
    std::stringstream str_stream(input);
    std::string temp;

    while (getline(str_stream, temp, delim))
    {
        tokens.push_back(temp);
    }

    return tokens;
}

std::vector<std::string> regex_searcher(const std::string& input, std::regex re)
{
    std::vector<std::string> ret;
    std::sregex_iterator begin(input.begin(), input.end(), re);
    std::sregex_iterator end;

    for (std::sregex_iterator it = begin; it != end; it++)
    {
        std::smatch match = *it;
        std::string match_str = match.str();
        ret.push_back(match_str);
    }

    return ret;
}

void program_terminate(std::string&& msg)
{
    std::cout << msg << std::endl;
    exit(1);
}

bool is_integer(std::string& line, int base)
{
    char* p;
    std::strtol(line.c_str(), &p, base);
    return *p == 0;
}

int to_integer(std::string& str, int base)
{
    char* end;
    return std::strtol(str.c_str(), &end, base);
}

int copy_code_integer(size_t& offset)
{
    offset++;
    int ret;
    memcpy(&ret, Debugger::get_instance().get_code_segment() + offset,
           sizeof(int));
    offset += sizeof(int) - 1;
    return ret;
}
