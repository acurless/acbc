.TH man 1 "11 December 2019" "1.0" "acbc man page"
.SH NAME
    acbc - Interpreter for acbc, a stack only 32-bit bytecode

.SH SYNOPSIS
.B acbc
.I <filename>

.SH DESCRIPTION
acbc is a register-less bytecode with 32 bit words. Instructions exist supporting
single byte operations and word operations (32 bits).

The acbc interpreter accepts a single acbc program file, and any number of acbc
library files. For details on the acbc syntax, see
.I acbc_opcodes(7).
At a minimum, an acbc program must consist of a
.BR main
symbol and at least one valid instruction. The last instruction to execute in any
program
.B must
be either a
.B ASSRT
or a
.B HALT
instruction. Symbols are resolved at runtime and can be used to call functions
and resolve jumps. See
.I EXAMPLES
for an example of how to define symbols.

The interpreter loads the standard library, contained in a file called
.B stdlib.acbc
distributed with the interpreter by default.
To use the standard library, simply call a symbol defined within it. Symbols
located in the standard library are automatically resolved by the interpreter.
It is undefined behavior to override a symbol defined in the standard library.

.SH CONCEPTS
.SS MEMORY
The acbc virtual machine provides a single, contiguous block of memory, divided
into two segments, a stack and a heap. The stack begins at offset 0 and grows
upwards. The heap begins at the top of the stack, and also grows upwards. The
stack is used to store variables and information about function and thread context.
The heap is fully available for use by programmers.

The relevant instructions for interacting with stack and heap memory are
.B LDI
and
.B STRI.
These instructions interact with absolute memory offsets placed on the stack. See
.I acbc_opcodes(7)
for details.  Additionally, the instruction
.B HSTRT
pushes the starting address of the heap to the stack. From there, offsets can be
added or subtracted to calculate the position of heap values.

.SS MULTI-THREADING
Simple multithreading is available. Currently, there are two revelant instructions,
.B THRD
and
.B JOIN. All threads share the same stack space, so be careful when spinning up
many threads at once in a single program.

The
.B THRD
instruction will create a new thread and execute the subsequent symbol in it. This
symbol should be written so it exposes any relevant return information via a
.B RET
instruction. Additional functions may be called, in this symbol. This instruction
returns a 32-bit thread id, which will be used later to join the thread.

The
.B JOIN
instruction will sleep the calling thread until the completion of a thread with the
given thread id. After this instruction is done executing, the return value (if any)
of the joined thread will be at the top of the stack.

Additionally, the
.B HALT
instruction will block until all threads have completed, so be sure to write each
thread to terminate correctly.

.SS FLOATING POINT
ACBC supports a float type, alias
.B F.
The type is 4 bytes wide and will work with any ACBC typed instruction. Some
instructions that do not make sense on floats, such as bitwise operations, are
implemented by performing the operation on the underlying representation of the
float.

.SH OPTIONS
.TP
.B -l <filename>
Link the given file name as a library. Its symbols will be available to the program
file. This flag can be specified any number of times, each with a separate library
file, however, only one file may contain the 'main' symbol.
.TP
.B -d
Start in debug mode. This flag should be specified only once. See the
.B DEBUGGER
section for details.
.TP
.B -c <output_file>
Compile the given acbc file along with any libraries and output a single acbc
bytecode file. All symbols will be resolved to absolute values. The interpreter
can then be called with the
.B -r
option to run the program.
.TP
.B -r
Runs an already compiled acbc file. Since symbols do not exist at runtime, the
.B -l
and
.B -d
flags are incompatible with this file. Create the bytecode file with the
.B -c
option described above.
.I IMPORTANT:
In order for a file created with the
.B -c
option to execute correctly, it must either begin with the "main" function or
begin with a
.B JMP
to the main function. This limitation does not exist when running the interpreter
in conventional mode (without the
.B -c
or
.B -r
arguments).
.TP
.B <filename>
acbc takes a string file name. This file must be a valid acbc bytecode file.
The interpreter will ignore newlines and tabs, however, individual op-codes must
be delimited with at least one space. For a list of valid op-codes,
see acbc_opcodes(7).

.SH EXAMPLES
.TP
Symbol Resolution and basic example program:

function:
    PUSH I 10
    PUSH I 20
    ADD I
    RET I

main:
    CALL sym.function 0
    PRNT I
    HALT

The above program demonstrates symbol resolution. Execution begins at the
.I main
symbol. The CALL instruction causes execution to jump to the symbol
.I function
The 0 argument to CALL indicates the presence of 0 arguments to the function.
The main function will continue to execute after called function is completed.

.SH DEBUGGER
The acbc interpreter includes a built-in debugger that can be activated using the
.B -d
command line option. When activate, a break point will be set on the first instruction
to be executed. The user will see the debugger CLI (Command Line Interface) appear.
The prompt character '>' indicates the presence of the CLI. From this interface,
type the question mark character '?' to see a help menu. Each command may also
have a subcommands, and their help menus may be access by appending a '?' character,
for example, to see the 'w' subcommand help, type 'w ?' which one space between
the 'w' command and the '?' character. The following options are available:

.TP
.B a
Analyze the loaded program. This command will output a disassembled version
of the program suitable for reference it its source is not available, or it
it was modified during debugging. In addition to the disassemly, a display of
the top 64 bytes of the stack is printed, and the current values of the program
counter, stack pointer and frame pointer. This command takes no arguments.

.TP
.B w
Write to a supported location. Supported locations include
    p - The program counter (instruction pointer).
    f - The frame pointer
    t - the sTack pointer
    s - The stack
    c - Program code

To write to the first three locations, execute the 'w' command with the correct
sub-argument and an integer value that should be written. Be careful to set pointers
to valid offsets, otherwise the running program may crash.

For the last two locations, the stack and the program code, there is one additional
argument. The 'w' command is executed with the correct sub-argument, followed by
the offset that should be written to and then the value that should be written.
For example, to write a '1' to the first byte of the stack, execute
.I w s 0 1

.TP
.B p
Dump supported values for viewing. Provide the appropriate sub-argument:
    s - Dump the stack. (Yes, the whole stack!)
    b - List the currently active breakpoints
    p - Display the program counter
    f - Display the frame pointer
    t - Display the stack pointer
    c - Print the loaded program code
    y - Display the symbol table

.TP
.B d
Adds or removes breakpoints, and runs/steps through the program. Provide the appropriate sub-argument:
  b - Set a breakpoint.
  B - Remove a breakpoint.
  c - Run the program until the next breakpoint is hit, or the program halts.
  s - Step one instruction into the program.

Additional information about each command is shown below:

.TP
.B b
Set a breakpoint. A breakpoint will case the program to halt
.B before
executing the instruction it has been applied to. A breakpoint may be applied
to any executable instruction, but not to offsets included in the code, such as a
JUMP offset. The
.I b
command takes a single argument, the index of the instruction to break on.

.TP
.B B
Remove a breakpoint. This command requires that a breakpoint exists at the given
index.

.TP
.B s
Step one instruction. This command will effectively set a breakpoint at the next
instruction to be executed, then advances to that instruction.
.I s
takes no arguments.

.TP
.B c
This command causes program execution to continue until the next breakpoint is
reach.

.TP
.B v
Initialize the visual mode of the debugger. All commands listed above can be run
in visual mode by pressing
.B :
to initialize the command prompt. Visual mode offers an illustration of the active
debugging session. Pressing
.B q
in visual mode returns the debugger back to the command line interface.

.TP
.B q
Terminate the program.

.SH SEE ALSO
.BR acbc_opcodes (7)

.SH AUTHOR
Adrian Curless (awcurless@wpi.edu)

Remy Kaldawy (remykaldawy@gmail.com)
