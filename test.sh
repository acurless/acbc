#!/bin/sh
#
# Very simple script to run some basic tests automatically.
# Manual testing should also be done for new or changed features
#
# Failed test cases should cause the script to exit with code -1.
#

./acbc examples/add.acbc || exit -1

./acbc examples/functions.acbc || exit -1

./acbc examples/fib.acbc || exit -1

./acbc -l stdlib.acbc examples/using_stdlib.acbc || exit -1

hello_str=$(./acbc -l stdlib.acbc examples/hello_world.acbc)
if [ "Hello World." != "$hello_str" ] ;
then
    echo "Failed puts test";
    exit -1;
fi

io_str="Testing!"
io_out=$(echo "$io_str" | ./acbc -l stdlib.acbc examples/tests/io_test.acbc)
if [ "> $io_str" != "$io_out" ] ;
then
    echo "Failed IO test";
    exit -1;
fi

thread_str="20000
-10"
thread_out=$(./acbc examples/multithread.acbc)
if [ "$thread_str" != "$thread_out" ] ;
then
    echo "Failed multithreading test";
    exit -1;
fi

heap_str="99"
heap_out=$(./acbc examples/heap.acbc)
if [ "$heap_str" != "$heap_out" ] ;
then
    echo "Failed heap test";
    exit -1;
fi

float_out=$(./acbc examples/tests/float_test.acbc)
float_exp="12202.476562"
if [ "$float_out" != "$float_exp" ] ;
then
    echo "Failed float test";
    exit -1;
fi

round_out=$(./acbc examples/tests/round.acbc)
round_exp="100
101
101"
if [ "$round_out" != "$round_exp" ] ;
then
    echo "Failed rounding test";
    exit -1;
fi

echo "Done. All tests passed."
