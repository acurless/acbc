#include "VirtualMemory.hpp"
#include <cerrno>
#include <cstring>
#include <iostream>

VirtualMemory::VirtualMemory(const size_t stack_len, const size_t heap_len)
    : len(stack_len + heap_len), stack_len(stack_len), heap_len(heap_len),
      base(new uint8_t[len])
{
}

VirtualMemory::~VirtualMemory()
{
    delete[] base;
}

uint8_t& VirtualMemory::operator[](const size_t& idx) const
{
    return at(idx);
}

uint8_t& VirtualMemory::at(const size_t& idx) const
{
    if (idx >= len)
    {
        std::cerr << "Invalid access to acbc virtual memory" << std::endl;
        exit(-EACCES);
    }
    else
    {
        return base[idx];
    }
}

void VirtualMemory::set_byte(const size_t offset, const uint8_t byte)
{
    base[offset] = byte;
}

uint8_t* VirtualMemory::add(const size_t offset) const
{
    if (offset < len)
    {
        return base + offset;
    }
    else
    {
        std::cerr << "Added off the end of allocated memory" << std::endl;
        exit(-EACCES);
    }
}

void VirtualMemory::setmem(size_t offset, uint8_t data, size_t data_len)
{
    if (offset + data_len > this->len)
    {
        std::cerr << "Would write off end of memory" << std::endl;
        exit(-EACCES);
    }
    else
    {
        memset(base + offset, data, data_len);
    }
}

void VirtualMemory::cpymem(size_t offset, const void* src, size_t n)
{
    if (offset + n > len)
    {
        std::cerr << "Would write off end of memory" << std::endl;
        exit(-EACCES);
    }
    else
    {
        memcpy(base + offset, src, n);
    }
}
