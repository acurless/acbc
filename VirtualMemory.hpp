#ifndef _VIRTUAL_MEMORY_H
#define _VIRTUAL_MEMORY_H

#include <cstdint>
#include <cstdlib>

/**
 * Virtual memory for ACBC. Implements bounds checking and other protections.
 */
class VirtualMemory
{
public:
    /**
     * Create an instance of the VirtualMemory holder.
     * @param stack_len The length of the stack, in bytes.
     * @param heap_len The length of the heap, in bytes.
     */
    VirtualMemory(const size_t stack_len, const size_t heap_len);

    VirtualMemory(const VirtualMemory&) = delete;
    VirtualMemory& operator=(const VirtualMemory&) = delete;

    /**
     * Free the allocated memory.
     */
    virtual ~VirtualMemory();

    uint8_t& operator[](const size_t& idx) const;
    uint8_t& at(const size_t& idx) const;
    void set_byte(const size_t offset, const uint8_t byte);
    uint8_t* add(const size_t offset) const;
    void setmem(size_t offset, uint8_t data, size_t data_len);

    void cpymem(size_t offset, const void* src, size_t n);

private:
    const size_t len;
    const size_t stack_len;
    const size_t heap_len;
    uint8_t* base;
};

#endif
