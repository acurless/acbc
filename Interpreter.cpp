#include "DebugExecutor.hpp"
#include "ProductionExecutor.hpp"
#include "ProgramLoader.hpp"
#include "Vm.hpp"
#include "code_loader/VectorCodeLoader.hpp"
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <string>
#include <vector>

void print_help(const char* program)
{
    printf("%s - A registerless bytecode with 32 bit words\n", program);
    printf("Options:\n");
    printf("\t-d                  Debug Mode\n");
    printf("\t-l <lib.acbc>       Link the library file\n");
    printf("\t-c <output_file>    Read an acbc file and output processed "
           "bytecode in raw form.\n");
    printf("\t-r                  Read a compiled file produced with -c and "
           "execute it.\n");
    printf("\t-h                  Show this help text\n");
    printf("Arguments:\n");
    printf("\t<file.acbc>         A single acbc program to be executed\n");
    printf("Type 'man 7 acbc' for details\n");
}

void assemble(std::string& source_file, std::string& library_file,
              ProgramLoader& loader, CodeLoader& code)
{
    if (!library_file.empty())
    {
        loader.load(library_file, code);
    }

    loader.load(source_file, code);
    loader.link(code);
}

void write_raw_bytecode(CodeLoader& code, std::string& output_filename)
{
    std::ofstream output;
    output.open(output_filename, std::ofstream::out);

    for (uint8_t byte : code)
    {
        output << byte;
    }
    output.close();
}

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        print_help(argv[0]);
        return -1;
    }

    uint8_t debug_mode = 0;
    uint8_t compile_only = 0;
    uint8_t run_only = 0;
    std::string library_file;
    std::string output_filename;

    int c = 0;
    while ((c = getopt(argc, argv, "dhrc:l:")) != -1)
    {
        switch (c)
        {
        case 'd':
            debug_mode = 1;
            break;
        case 'l':
            library_file = optarg;
            break;
        case 'r':
            run_only = 1;
            break;
        case 'c':
            compile_only = 1;
            output_filename = optarg;
            break;
        default:
            print_help(argv[0]);
            exit(-1);
        }
    }

    ProgramLoader loader;
    std::string source_file{argv[optind]};
    VectorCodeLoader code;

    if (run_only)
    {
        loader.load_raw(source_file, code);
        ProductionExecutor ex(code, 0);
        ex.execute();
    }
    else if (compile_only)
    {
        printf("writing raw acbc\n");
        assemble(source_file, library_file, loader, code);
        write_raw_bytecode(code, output_filename);
    }
    else
    {
        assemble(source_file, library_file, loader, code);
        const size_t main_addr = loader.markers["main"];

        if (debug_mode)
        {
            printf("Debug Mode\n");
            DebugExecutor ex(code, main_addr, loader.markers);
            ex.execute();
        }
        else
        {
            ProductionExecutor ex(code, main_addr);
            ex.execute();
        }
    }

    return 0;
}
