#include "ACBCThreadManager.hpp"
#include <chrono>

ACBCThreadManager& ACBCThreadManager::get_instance()
{
    static ACBCThreadManager inf;
    return inf;
}

ACBCThreadManager::ACBCThreadManager() : thread_count(1)
{
}

uint32_t ACBCThreadManager::new_thread(std::unique_ptr<Vm>&& vm,
                                       std::future<int32_t>&& executor)
{
    std::lock_guard<std::mutex> l(this->thread_lock);
    this->threads.emplace(this->thread_count,
                          std::make_tuple(std::move(vm), std::move(executor)));
    uint32_t thread_id = this->thread_count;
    this->thread_count++;
    return thread_id;
}

int32_t ACBCThreadManager::join_thread(uint32_t thread_id)
{
    std::lock_guard<std::mutex> l(this->thread_lock);

    auto target = threads.find(thread_id);
    if (target != threads.end())
    {
        std::future<int32_t>& f = std::get<1>(target->second);

        f.wait();
        int ret = f.get();
        this->threads.erase(target);

        return ret;
    }
    else
    {
        program_terminate("Unknown thread with id " +
                          std::to_string(thread_id));
        return 0;
    }
}

std::vector<uint32_t> ACBCThreadManager::active_threads() const
{
    std::vector<uint32_t> active;
    active.reserve(this->threads.size());

    for (auto it = this->threads.begin(); it != this->threads.end(); ++it)
    {
        active.push_back(it->first);
    }
    return active;
}

uint32_t ACBCThreadManager::calculate_thread_stack_start(uint32_t main_sp)
{
    std::lock_guard<std::mutex> l(this->thread_lock);
    uint32_t ret = 0;
    if (this->thread_start_sp.size() == 0)
    {

        if (main_sp + (Vm::STACK_SIZE / 6) < Vm::STACK_SIZE)
        {
            ret = main_sp + (Vm::STACK_SIZE / 6);
        }
        else
        {
            program_terminate("No room for thread stack");
        }
    }
    else
    {
        auto last = (this->thread_start_sp.end() - 1);
        ret = *last + (2 * (Vm::STACK_SIZE / 6));
        if (ret > Vm::STACK_SIZE)
        {
            program_terminate("No room for thread stack");
        }
    }

    this->thread_start_sp.push_back(ret);
    return ret;
}
