#ifndef _ACBC_THREAD_MANAGER_H
#define _ACBC_THREAD_MANAGER_H

#include "Util.hpp"
#include "Vm.hpp"
#include <future>
#include <map>
#include <memory>
#include <mutex>

class ACBCThreadManager
{
public:
    static ACBCThreadManager& get_instance();

    ACBCThreadManager(const ACBCThreadManager&) = delete;
    ACBCThreadManager& operator=(const ACBCThreadManager&) = delete;

    uint32_t new_thread(std::unique_ptr<Vm>&& vm,
                        std::future<int32_t>&& executor);

    int32_t join_thread(uint32_t thread_id);

    std::vector<uint32_t> active_threads() const;

    uint32_t calculate_thread_stack_start(uint32_t main_sp);

private:
    std::map<uint32_t, std::tuple<std::unique_ptr<Vm>, std::future<int32_t>>>
        threads;
    std::vector<uint32_t> thread_start_sp;
    std::mutex thread_lock;
    uint32_t thread_count;

    ACBCThreadManager();
};

#endif
