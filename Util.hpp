#ifndef _UTIL_H
#define _UTIL_H

#include <regex>
#include <string>
#include <vector>

constexpr unsigned HEX = 16;

std::vector<std::string> tokenizer(const std::string& input, char delim);

std::vector<std::string> regex_searcher(const std::string& input,
                                        std::regex re);

void program_terminate(std::string&& msg);

bool is_integer(std::string& line, int base = HEX);

int to_integer(std::string& str, int base = HEX);

int copy_code_integer(size_t& offset);

#endif
